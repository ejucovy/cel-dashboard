Model.new(:dashboard_backup, 'Backup CEL Dashboard postgresql db') do

  split_into_chunks_of(1000)

  database PostgreSQL do |db|
    # To dump all databases, set `db.name = :all` (or leave blank)
    db.name               = "XXXXX"
    db.username           = "XXXXX"
    db.password           = "XXXXX"
    db.host               = "localhost"
    db.port               = 5432
  end

  Compressor::Custom.defaults do |compressor|
    compressor.command = 'xz'
    compressor.extension = '.xz'
  end

  encrypt_with OpenSSL do |encryption|
    encryption.password_file = '/home/admin/cc/shared/config/backup-key.txt'
    encryption.base64        = true
    encryption.salt          = true
  end

  store_with S3 do |s3|
    s3.keep = 30

    # AWS Credentials
    s3.access_key_id     = "XXXXX"
    s3.secret_access_key = "XXXXX"

    s3.region            = 'us-east-1'
    s3.bucket            = 'XXXXX'
    s3.path              = '/backups'

    s3.max_retries       = 10
    s3.retry_waitsec     = 30

  end

  notify_by Mail do |mail|
    mail.on_success           = true
    mail.on_warning           = true
    mail.on_failure           = true

    mail.from                 = 'XXXXX'
    mail.to                   = 'XXXXX'
    mail.address              = 'localhost'
    mail.port                 = 25
    mail.domain               = 'XXXXX'
  end

end
