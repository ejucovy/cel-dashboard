class PagesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @filter_params = filter_params()
    if current_client.super_admin? && Client.count > 1
      @rows = []
      @tsdata = {}.to_json
    else
      @rows = Summary::Query.page_summary(current_client, @filter_params, tz: current_user.tz)
      @summaryChartData = Yajl::Encoder.encode(@rows)
    end
  end
  
  def detail
    @filter_params = filter_params()
    @page = AkPage.includes(:tags).find(@filter_params[:page_id])
    @rows = Summary::Query.page_time_series(current_client, @filter_params, json: false, tz: current_user.tz, order: 'DESC')
    @chartData = Yajl::Encoder.encode(@rows.to_a.reverse)
  end

  def action_sources
    @page = AkPage.includes(:tags).find(filter_params[:page_id])
    @rows = Summary::Query.page_sources(current_client, filter_params[:page_id], 'action', 25)
    @fields = AkActionfield.joins(action: :page).where(core_action: {page_id: @page}).count(:all, :group => "core_actionfield.name")
    @chartData = Yajl::Encoder.encode(@rows)
  end

  def user_sources
    @page = AkPage.includes(:tags).find(filter_params[:page_id])
    @rows = Summary::Query.page_sources(current_client, filter_params[:page_id], 'user', 25)
    #!@fields = AkUserfield.joins(action: :page).where(core_action: {page_id: @page}).count(:all, :group => "core_userfield.name")
    @chartData = Yajl::Encoder.encode(@rows)
  end

  def time_series
    @tsdata = Summary::Query.page_time_series(current_client, filter_params, tz: current_user.tz, json: true)
    render :json => @tsdata
  end

  private

  def filter_params
    p = params.permit(:page_ids,
                      :page_id,
                      :page_type,
                      :activity_start_dt,
                      :activity_end_dt,
                      :domain_ids,
                      :tag_ids,
                      :mailing_ids,
                      :user_source_ids,
                      :action_source_ids,
                      :limit).select{|k,v| v.present? }
    p[:page_ids] = p[:page_id] if p[:page_id] # A cheat/convenience for the query functions.
    p.with_indifferent_access
  end

end
