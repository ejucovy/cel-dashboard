class UsersController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource :except => [:create]

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.where(client_id: current_client.id)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.client = current_client
    @user.add_role :user if @user.roles.blank?
    @user.active = true

    if @user.save
      flash[:notice] = "Successfully created User."
      redirect_to users_path
    else
      render :action => 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    puts user_params
    if @user.update_attributes(user_params)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.active = false
      user.save!
      redirect_to users_path, :notice => "User deactivate."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end

  def user_params
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    params.require(:user).permit(:id, :name, :email, :password, :password_confirmation, :role_ids, :active)
  end
end