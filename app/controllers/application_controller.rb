require 'pp'
require 'summary/query'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :current_client

  before_action :set_tenant,        except: :record_page_view
  before_action :available_clients, except: :record_page_view
  after_action :reset_tenant,       except: :record_page_view

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  def current_client
    return nil unless current_user
    return @current_client if defined?(@current_client)

    if current_user.super_admin? == true && Client.count > 1
      if session[:current_client_id]
        @current_client = Client.find(session[:current_client_id])
      else
        session[:current_client_id] = current_user.client_id
        @current_client = current_user.client
      end
    else
      @current_client = current_user.client
    end

    return @current_client
  end

  def available_clients
    if current_client
      @available_clients = Client.where("active = true AND id > 0").order(:name)
    else
      @available_clients = []
    end
  end

  private

  def set_tenant
    if current_client && current_client.cc_schema.present?
      Apartment::Database.switch(current_client.cc_schema) 
    end
  end

  def reset_tenant
    Apartment::Database.switch(nil)
  end


end
