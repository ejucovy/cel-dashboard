require 'summary/populate/page_views'

class PageViewsController < ApplicationController

  def record_page_view
    if params[:d].present? && params[:pn].present?
      client = Client.lookup_by_ak_domain(params[:d])
      if client
        Apartment::Database.switch(client.cc_schema_name)
        page_id = AkPage.id_from_name(params[:pn])
        if page_id > 0
          referrer_id = ReferrerUrl.find_or_create!(params[:r])
          browser_spec = BrowserSpec.find_or_create!(browser_params)
          Summary::Populate.record_page_view(client, page_id, referrer_id, browser_spec, params[:akid])
        end
        Apartment::Database.switch(nil)
      end
    end

    send_file "#{Rails.root}/public/1.gif", type: 'image/gif', disposition: 'inline'
  end

  private

  def browser_params
    params.permit(:p, :os, :b, :bv, :s, :o).with_indifferent_access
  end

end