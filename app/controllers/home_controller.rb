class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @fparams = filter_params
    if current_client.super_admin? && Client.count > 1
      @time_frame    =  'TM'
      @curr_lists    = []
      @all_lists     = []
      @mailing_rows  = []
      @page_rows     = []
      @overview_data = []
      @stats         = {}
    else
      @time_frame    = filter_params[:time_frame] || 'TM'
      @curr_lists    = params[:list_ids] || AkList.default.id
      @all_lists     = AkList.all()
      @mailing_rows  = Summary::Query.mailing_summary(current_client, @fparams, tz: current_user.tz)
      @page_rows     = Summary::Query.page_summary(current_client, @fparams, tz: current_user.tz, limit: 5)
      @overview_data = Summary::Query.list_activity(current_client, list_ids: @curr_lists, time_frame: @time_frame, tz: current_user.tz, json: true)
      @stats         = Summary::Query.list_stats(current_client, list_id: @curr_lists, time_frame: @time_frame)
    end
  end

  def set_current_client
    if current_user.super_admin? && params[:client_id]
      session[:current_client_id] = params[:client_id]
      @current_client = Client.find(session[:current_client_id])

      redirect_to(:back) and return
    end
    redirect_to '/'
  end

  private

  def filter_params
    p = params.permit(:mailing_ids,
                      :mailing_id,
                      :mailing_start_dt,
                      :mailing_end_dt,
                      :domain_ids,
                      :list_ids,
                      :tag_ids,
                      :page_ids,
                      :activity_start_dt,
                      :activity_end_dt,
                      :user_source_ids,
                      :limit,
                      :time_frame).select{|k,v| v.present? }
    p[:mailing_ids] = p[:mailing_id] if p[:mailing_id] # A cheat/convenience for the query functions.
    now = Date.today
    p[:mailing_start_dt] = now - 14
    p.with_indifferent_access
  end

end
