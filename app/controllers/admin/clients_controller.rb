class Admin::ClientsController < ApplicationController

  before_filter :authenticate_user!
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.where('active = true')
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
  end

  # GET /clients/new
  def new
    @client = Client.new
    @client.master_user = User.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    init_passwd = Devise.friendly_token

    ma = master_user_params()
    ma[:password] = init_passwd
    ma[:password_confirmation] = init_passwd
    ma[:time_zone] = client_only_params[:time_zone]

    @client = Client.new(client_only_params)

    Client.transaction do
      respond_to do |format|
        if @client.save
          @client.master_user = User.new(ma)
          @client.master_user.client = @client
          @client.master_user.save!
          @client.save!
          @client.master_user.add_role :admin
          @client.master_user.active = true; #Cat: Sanity check

          format.html { redirect_to admin_client_path(@client), notice: 'Client was successfully created.' }
          format.json { render action: 'show', status: :created, location: @client }
        else
          format.html { render action: 'new' }
          format.json { render json: @client.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to admin_client_path(@client), notice: 'Client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.active = false
    @client.save!
    respond_to do |format|
      format.html { redirect_to admin_clients_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.includes(:master_user).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:active, :admin, :name, :domain, :ak_domain, :ak_schema_name, :cc_schema_name, :time_zone, :page_significance_threshold, :email_action_threshold, :email_action_threshold_time, :default_timeframe, :master_user_attributes => [:id, :name, :email])
    end

    def client_only_params
      params.require(:client).permit(:active, :admin, :name, :domain, :ak_domain, :ak_schema_name, :cc_schema_name, :time_zone, :page_significance_threshold, :email_action_threshold, :email_action_threshold_time, :default_timeframe)
    end

    def master_user_params
      params.require(:client).permit(:master_user_attributes => [:name, :email])[:master_user_attributes]
    end

end
