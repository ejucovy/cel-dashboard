class MailingsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @filter_params = filter_params

    if current_client.super_admin? && Client.count > 1
      @rows = []
      @tsdata = {}.to_json
    else
      @rows = Summary::Query.mailing_summary(current_client, @filter_params, tz: current_user.tz)
      @summaryChartData =  Yajl::Encoder.encode(Summary::Query.mailing_summary(current_client, @filter_params, tz: current_user.tz, include_subjects: false))
      get_mailing_aggregate
    end
  end

  def comparison
    @filter_params = filter_params
    @durations = ['1 hour'] + (2..24).collect{|x| "#{x} hours"} + (2..7).collect{|x| "#{x} days"} + ['2 weeks', '3 weeks', '4 weeks', '2 months', '3 months']
    @filter_params[:duration] = '24 hours' if @filter_params[:duration].blank?
    @rows = @cumulative_rows = []

    if request.post?
      # Retrieve the data by period so it can be graphed, 
      # then calculate totals per mailing Id for the table
      @rows = Summary::Query.mailing_comparison(current_client, @filter_params)
      @cumulative_rows = calc_cumulative(@rows, @filter_params[:mailing_ids])
    end
  end
  
  def detail
    @mailing = AkMailing.find(filter_params[:mailing_id])
    params[:mailing_start_dt] = @mailing.started_at
    @rows = Summary::Query.mailing_summary(current_client, filter_params, tz: current_user.tz)
    @summaryChartData =  Yajl::Encoder.encode(Summary::Query.mailing_summary(current_client, filter_params, tz: current_user.tz, include_subjects: true))
    get_mailing_aggregate
  end

  def interval
    @rows = Summary::Query.mailing_time_series(current_client, filter_params, json: false, tz: current_user.tz, interval: 900, period: 1)
    @tsdata =  Yajl::Encoder.encode(@rows)
    @mailing = AkMailing.find(filter_params[:mailing_id])
  end

  def email_domains
    @domain_rows = Summary::Query.mailing_by_email_domain(current_client, filter_params[:mailing_id])
    @mailing = AkMailing.find(filter_params[:mailing_id])
  end

  def user_sources
    @sources = Summary::Query.mailing_sent_by_source(current_client, filter_params[:mailing_id], 25)
    @mailing = AkMailing.find(filter_params[:mailing_id])
  end

  def links
    @links = Summary::Query.mailing_clicks_by_link(current_client, filter_params[:mailing_id], limit: 15)
    @mailing = AkMailing.find(filter_params[:mailing_id])
  end

  def time_series
    @tsdata = Summary::Query.mailing_time_series(current_client, filter_params, json:true, tz: current_user.tz)
    render :json => @tsdata
  end

  private

  # TODO: Probably makes sense to allow selective querying by period, 
  # or we should simply making this one query.
  def get_mailing_aggregate
    @week_agg  = Summary::Query.mailing_aggregate(current_client, 'week',    json: true)
    @month_agg = Summary::Query.mailing_aggregate(current_client, 'month',   json: true)
    @qtr_agg   = Summary::Query.mailing_aggregate(current_client, 'quarter', json: true)
    @year_agg  = Summary::Query.mailing_aggregate(current_client, 'year',    json: true)
  end

  # Hackish, but expedient...
  def calc_cumulative(rows, mailing_id)
    ccols = ["opens", "clicks", "actions", "unsubs", "unsub_complaints", "unsub_bounces", "donations", "revenue"]
    crows = []

    mailing_ids = @filter_params[:mailing_ids].split(/[, ]/).reject(&:blank?).sort

    mailing_ids.each do |mid|
      trows = rows.select{|x| x['mailing_id'] == mid}
      next if trows.empty?
      cr = {}
      cr['mailing_id'] = mid
      cr['sent'] = trows.first['sent']
      ccols.each do |col|
        cr[col] = 0
      end
      trows.each do |tr|
        ccols.each do |col|
          cr[col] += tr[col].to_i
        end
      end
      crows << cr
    end
    crows
  end

  def filter_params
    p = params.permit(:mailing_ids, 
                      :mailing_id, 
                      :mailing_start_dt,
                      :mailing_end_dt,
                      :domain_ids,  
                      :domain_keys,                    
                      :tag_ids,
                      :page_ids,
                      :user_source_ids,
                      :duration,
                      :limit).select{|k,v| v.present? }
    p[:mailing_ids] = p[:mailing_id] if p[:mailing_id] # A cheat/convenience for the query functions.
    p.with_indifferent_access
  end

end
