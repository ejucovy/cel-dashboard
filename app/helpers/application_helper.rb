module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def pct(n, d, precision = 2)
    return '' if n.blank? || d.blank?
    return '0%' if n.to_i == 0 || d.to_i == 0

    ("%0.#{precision}f" % ((n.to_f / d.to_f) * 100)) + '%'
  end

  def page_significance(page)
    if page['actions'].to_f >= (current_client.page_significance_threshold * page['action_avg'].to_f)
      'significant'
    else
      'not-significant'
    end
  end

  def last_sync
    return '' if !current_client || current_client.super_admin?

    sl = SyncLog.last_successful(current_client)
    "Last sync: #{time_ago_in_words(sl.updated_at)} ago"
  end

end
