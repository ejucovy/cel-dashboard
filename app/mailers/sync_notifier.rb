class SyncNotifier < ActionMailer::Base  
  default :from => "tech+ak_syncer@engagementlab.org"

  def email_report(db, message)
    @message = message
    mail(:to => ['tech@engagementlab.org'],
         :subject => "CEL Dashboard: AK sync report: #{db}")
  end

end  
