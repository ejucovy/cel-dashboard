class Client < ActiveRecord::Base
  belongs_to :master_user, :class_name => :User, :foreign_key => :master_user_id, :autosave => true
  has_many :users, :inverse_of => :client

  accepts_nested_attributes_for :master_user

  validates_uniqueness_of :ak_schema_name    #, unless: :super_admin?
  validates_uniqueness_of :cc_schema_name    #, unless: :super_admin?

  def super_admin?
    self.id == 1
  end

  def ak_schema
    ak_schema_name
  end

  def cc_schema
    cc_schema_name
  end

  def tz_identifier 
    ActiveSupport::TimeZone.zones_map[time_zone].tzinfo.identifier
  end
  alias_method :tz, :tz_identifier

  def tz_offset
    ActiveSupport::TimeZone.zones_map[time_zone].formatted_offset
  end

  def Client.lookup_by_ak_domain(domain)
    c = self.cache_read(domain)

    if c.nil?
      c = Client.select(:id, :cc_schema_name).where(ak_domain: domain, active: true).first
      if c
        c
        self.cache_write(domain, c)
      else
        c = nil
      end
    end

    return c
  end

  private

  def self.cache_read(key)
    Rails.cache.read("#{self.to_s}:#{key}")
  end

  def self.cache_write(key, data)
    Rails.cache.write("#{self.to_s}:#{key}", data)
  end



end
