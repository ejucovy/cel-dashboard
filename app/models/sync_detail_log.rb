class SyncDetailLog < ActiveRecord::Base
  self.table_name = "sync_detail_log"

  belongs_to :sync_log
end
