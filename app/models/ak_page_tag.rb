class AkPageTag < ActiveRecord::Base
  self.table_name = "core_page_tags"
  belongs_to :tag, :class_name  => :AkTag,  :foreign_key => :tag_id
  belongs_to :page, :class_name => :AkPage, :foreign_key => :page_id
end
