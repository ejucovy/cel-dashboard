class EmailDomain < ActiveRecord::Base
  self.table_name = "domains"

def as_json(*args)
    super.tap { |hash| hash["name"] = hash.delete "domain" }
end

end