class BrowserSpec < ActiveRecord::Base

  def self.find_or_create!(p)

    begin

      spec = "p=#{p[:p]}&os=#{p[:os]}&b=#{p[:b]}&bv=#{p[:bv]}&s=#{p[:s]}&o=#{p[:o]}"

      bs = self.cache_read(spec)

      if bs.nil?
        bs = self.where(platform: p[:p], os: p[:os], browser: p[:b], browser_ver: p[:bv], screen: p[:s], orientation: p[:o]).limit(1).first
        if bs.nil?
          bs = self.new
          bs[:platform]    = p[:p]
          bs[:os]          = p[:os]
          bs[:browser]     = p[:b]
          bs[:browser_ver] = p[:bv]
          bs[:screen]      = p[:s]
          bs[:orientation] = p[:o]
          bs.save!
          self.cache_write(spec, bs) if bs
        end
      end

      return bs

    rescue => e
      Rails.logger.error e.message.to_s + "\n" + e.backtrace.join("\n")
      return nil
    end
  end

  private

  def self.cache_read(key)
    Rails.cache.read("#{self.to_s}:#{key}")
  end

  def self.cache_write(key, data)
    Rails.cache.write("#{self.to_s}:#{key}", data)
  end

end
