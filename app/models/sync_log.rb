class SyncLog < ActiveRecord::Base
  self.table_name = "sync_log"

  has_many :details, :class_name => :SyncDetailLog


  def self.latest(client)
    SyncLog.where(schema_name: client.ak_schema).order(id: :desc).limit(1).first
  end

  def self.last_successful(client)
    SyncLog.where(schema_name: client.ak_schema, status: 'ok').order(id: :desc).limit(1).first
  end

end
