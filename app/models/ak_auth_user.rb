class AkAuthUser < ActiveRecord::Base
  self.table_name = "auth_user"
  has_many :mailings, :class_name => "AkMailing", :foreign_key => :submitter_id
end
