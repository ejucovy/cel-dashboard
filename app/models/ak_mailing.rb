class AkMailing < ActiveRecord::Base
  self.table_name = "core_mailing"
  has_many :subjects, :class_name => "AkMailingSubject", :foreign_key => :mailing_id
  belongs_to :author, :class_name => "AkAuthUser", :foreign_key => :submitter_id
  belongs_to :sender, :class_name => "AkAuthUser", :foreign_key => :queued_by_id
  belongs_to :page, :class_name => "AkPage", :foreign_key => :landing_page_id
  belongs_to :queuer, :class_name => "AkAuthUser", :foreign_key => :queued_by_id
  belongs_to :scheduler, :class_name => "AkAuthUser", :foreign_key => :scheduled_by_id

  has_and_belongs_to_many :tags, :class_name => :AkTag, :join_table => 'core_mailing_tags',  :foreign_key => :mailing_id, :association_foreign_key => :tag_id
end
