insertAfter = (newNode, referenceNode) ->
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)

collect = () ->
  d = {}

  try
    d.pn = window.actionkit.form.page.value
  catch e
    return null
  
  try
    d.d    = document.location.hostname
    d.r    = document.referrer
    d.akid = window.actionkit.context.args.akid

  b = $.pgwBrowser()
  d.p = 'desktop'
  if /phone|iPad|BlackBerry/i.test(b.os.name) || /Opera Mini/i.test(b.browser.name)
    d.p = 'mobile'
  else if /iPad|ChromeOS/i.test(b.os.name) || /Nexus 7|Nexus 10/i.test(b.userAgent)
    d.p = 'tablet'

  d.os = b.os.group
  d.b  = b.browser.group
  d.bv = b.browser.majorVersion
  d.s  = window.screen.width + 'x' + window.screen.height
  d.o  = b.viewport.orientation[0]

  qs = ''
  for k, v of d
    qs += k + '=' + encodeURIComponent(v) + '&'
  return qs


t = () ->
  qs = collect()
  return if qs == null

  u = 'https:' == document.location.protocol && 'https://' || 'http://'
  u += document.location.hostname == 'localhost' && ('localhost:' + document.location.port + '/pv/1.gif') ||  'dash.engagementlab.org/pv/1.gif'
  u += '?' + qs
  i = document.createElement('img')
  i.setAttribute('style', 'display:none;')
  i.src = u
  b = document.getElementsByTagName('body')[0]
  b.appendChild(i)

$ ->
  t()
