tsParse = (ts) ->
  [d, t] = ts.split(' ')
  [y, mon, d] = d.split('-')
  [h, min, s] = t.split(':')
  dt = new Date(y, mon, d, h, min, s)
  return dt


initDetailChart = () ->

  # after draw, use text() to select exact matches for 0 values
  # on measure charts.  need to make this less generalizable since
  # it really only applies to the condition in which we have
  # two charts stacked together.
  removeOriginPercent = () ->
    text = "0.00%"
    text2 = "0"
    zeroTick = $('.tick').filter () ->
       return $(@).text() == text || $(@).text() == text2
    zeroTick.remove()

  buildTooltip = (e, svg, series) ->
    console.log series
    #config so you don't have to read my silly code!
    config = {
      borderWidth: 5 #pixels
      borderColor: e.selectedShape.attr("fill")
      bgColor: '#FFF'
      font: 'sans-serif'
      fontColor: '#222'
      fontSize: '11px'
      width: 150
      height: 55
    }
    
    if $.inArray(e.seriesValue[0], ["Unsubs", "Unsub_Complaints", "Unsub_Bounces"]) >= 0
      unsub = true

    #Get the properties of the selected shape
    cx = parseFloat(e.selectedShape.attr("x"))
    cy = parseFloat(e.selectedShape.attr("y"))
    height = parseFloat(e.selectedShape.attr("height")) #don't get this.

    #Set the size and position of the tooltip
    x = if cx + config.width + 10 < svg.attr("width") then cx + 10 else cx - config.width - 20
    y = if cy - config.height / 2 < 0 then 25 else 150

    #Create a group for the tooltip objects
    #TODO: get out of global namespace icky!
    tooltip = series.chart._tooltipGroup = svg.append("g").attr("class", "tooltip-wrapper")

    #Add a rectangle surrounding the text pop
    tooltip.append("rect")  
    .attr("x", x + 140)
    .attr("y", y - 5)
    .attr("width", config.width)
    .attr("height", config.height)
    .attr("rx", 5)
    .attr("ry", 5)
    .style("fill", config.bgColor)
    .style("stroke", config.borderColor)
    .style("stroke-width", config.borderWidth);
    console.log e
    switch parseInt(e.xValue)
      when -4 then mid = "Year"
      when -3 then mid = "Quarter"
      when -2 then mid = "Month"
      when -1 then mid = "Week"
      else mid = e.xValue

    if unsub
      # TODO: i believe there is a bug in dimple that is preventing the eventArgs object from passing the height of the 
      # value as it's yValue when the height in negative (i.e. for unsub, complaints, and bounces).  will follow up/.
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + '-' + parseInt(e.selectedShape[0][0].attributes.height.nodeValue)/100.toFixed(2) + '%', 'Subject ID: ' + mid]
    else
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + (e.yValue * 100).toFixed(2) + '%', 'Subject ID: ' + mid]

    tooltip.append('text')
           .selectAll('tspan').data(text)
           .enter()
           .append('tspan')
           .attr('x', x + 152)
           .attr('y', (d,i) -> return y + ((i + 1) * 13))
           .text((d) -> return d)
           .style("font-family", config.font)
           .style("font-size", config.fontSize)
           .style("color", config.fontColor)

    dropDest = series._dropLineOrigin()
    offset = if series._isStacked() then 1 else width / 2

    if not series.y._hasCategories() and dropDest.x != null 
      series.chart._tooltipGroup.append("line")
           .attr("x1", (if cx < dropDest.x then cx + width else cx))
           .attr("y1", (if unsub then height else cy))
           .attr("x2", (if cx < dropDest.x then cx + width else cx))
           .attr("y2", (if unsub then height else cy))
           .style("fill", "none").style("stroke", e.selectedShape.attr("fill"))
           .style("stroke-width", 2).style("stroke-dasharray", ("3, 3"))
           .style("opacity", e.selectedShape.attr("opacity"))
           .transition()
           .delay(750 / 2)
           .duration(750 / 2)
           .ease("linear")
           .attr("x2", (if x < dropDest.x then dropDest.x - 1 else dropDest.x + 1))
        #Added 1px offset to cater for svg issue where a transparent
        #group overlapping a line can sometimes hide it in some browsers
        #Issue #10
  
  chartDuration = 800 #Length (ms) for chart to fully render

  removeTooltip = (e, svg, series) ->
    #TODO: cache objects in DOM if they've been instantiated so we only get
    #one instance of a tooltip rather than rebuild?  
    if series.chart._tooltipGroup != null
      series.chart._tooltipGroup.remove()


  addAggregateData = () ->
    for d of Data.aggregate
      # Do not include data if there were no emails sent for the period
      if Data.aggregate[d]['sent'] > 0
        Data.aggregate[d]['subject_id'] = d.toString()
        Data.aggregate[d]['started_at'] = "1999-12-31 11:59:59"
        Data.mailingDetailSummary.push(Data.aggregate[d])


  buildMailingListSummaryChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      for event in ["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"]
        if eventsWanted.indexOf(event) > -1
          count = parseInt(row[event.toLowerCase()])
          pct = parseInt(row[event.toLowerCase()]) / parseInt(row['sent'])
          pct = -pct if $.inArray(event,  ['Unsubs', 'Unsub_Bounces', 'Unsub_Complaints']) >= 0
        else
          count = 0
          pct = 0
         #for now, I'm hiding the aggregate.  I will update to show aggregate when # of subjects > 1
        if parseInt(row['subject_id']) != 0
        
          d = {}
          d['Mailing ID'] = row['mailing_id']
          d['Subject']    = row['subject']
          d['Subject ID'] = parseInt(row['subject_id'])
          d['Event']      = event
          d['Percent']    = pct
          d['Count']      = count
          d['Sent']       = parseInt(row['sent'])
          d['Started At'] = tsParse(row['started_at'])
          chartData.push(d)

    return chartData

  # TODO: Bug: If all metrics are unchecked, grouped bar effect breaks, 
  # and unsubs are charted directly under other metrics.
  # Not sure what the hack is here to fix this, since this is in itself a hack, 
  # but it might be a good time to revisit whether we want two separate charts.
  makeLegendInteractive = (chart, unsubChart, legend) ->
    # remove legend from chart, so that it doesn't update (i.e. remove metrics) when the chart updates
    chart.legends = []
    # grab all of our metric names to start
    filterValues = dimple.getUniqueValues(chart.data, "Event");

    legend.shapes.selectAll("rect").on "click", (e) -> 
      hide = false
      newFilters = [] #metrics that will be charted at the end
      metricClicked = e.aggField.slice(-1)[0]

      #TODO: Improve data structure for hidden data and associated metrics (for easy indexing)
      if typeof Data.hiddenMetricKeys == "undefined"
        Data.hiddenMetricKeys = [] 

      #loop through values that are visible, see if the one that clicked needs to be hidden
      filterValues.forEach (f) ->
        if (f == metricClicked) 
          hide = true #if hide, the item was already visible
          Data.hiddenMetricKeys.push(f)
          if typeof Data.hiddenOcaMetrics == "undefined" or typeof Data.hiddenUnsubMetrics == "undefined"
            #Grab the fake data and the real data from both charts, put them in separate caches.  
            Data.hiddenUnsubMetrics = dimple.filterData(unsubChart.data, "Event", metricClicked)
            Data.hiddenOcaMetrics = dimple.filterData(chart.data, "Event", metricClicked)
          else 
            Data.hiddenUnsubMetrics = Data.hiddenUnsubMetrics.concat(dimple.filterData(unsubChart.data, "Event", metricClicked))
            Data.hiddenOcaMetrics = Data.hiddenOcaMetrics.concat(dimple.filterData(chart.data, "Event", metricClicked))
        else
          newFilters.push(f)
      
      if hide 
        d3.select(this).style("opacity", 0.2)
      else
        d3.select(this).style("opacity", 0.8)
        if $.inArray(metricClicked, Data.hiddenMetricKeys) != -1
          Data.hiddenMetricKeys.splice($.inArray(metricClicked,Data.hiddenMetricKeys), 1)
        newFilters.push(metricClicked)
            
      filterValues = newFilters

      # if we are hiding a metric, filter it from the data and set that to the new chart data, then draw.
      # if we are unhiding a metric, remove the metric from the hidden cache and add to the chart data. then draw.
      if hide 
          unsubChart.data = dimple.filterData(unsubChart.data, "Event", filterValues)
          chart.data = dimple.filterData(chart.data, "Event", filterValues)
      else
        hiddenUnsubData = dimple.filterData(Data.hiddenUnsubMetrics, "Event", metricClicked)
        hiddenOcaData = dimple.filterData(Data.hiddenOcaMetrics, "Event", metricClicked)
        unsubChart.data = unsubChart.data.concat(hiddenUnsubData)
        chart.data = chart.data.concat(hiddenOcaData)
        Data.hiddenUnsubMetrics = dimple.filterData(Data.hiddenUnsubMetrics, "Event", Data.hiddenMetricKeys)
        Data.hiddenOcaMetrics = dimple.filterData(Data.hiddenOcaMetrics, "Event", Data.hiddenMetricKeys)
      renderChart(false)

  # From http://bl.ocks.org/mbostock/7555321
  # converts text node into corresponding tspans
  # TODO: put into some sort of chart helpers object
  wrap = (text, width) ->
    text.each ->
      text = d3.select(this)
      words = text.text().split(/\s+/).reverse()
      word = undefined
      line = []
      lineNumber = 0
      lineHeight = 1.1 # ems
      x = "6.5%"
      y = text.attr("y")
      dy = parseFloat(text.attr("dy"))
      tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em")
      while word = words.pop()
        line.push word
        tspan.text line.join(" ")
        if tspan.node().getComputedTextLength() > width
          line.pop()
          tspan.text line.join(" ")
          line = [word]
          tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word)
      return
    return

  replaceUnsubAxisLabels = (data, axis) ->
    window.labels = []
    data.splice(0,1) if parseFloat(data[0]['subject_id']) == 0 #get rid of aggregate

    compare = (a, b) ->
      if (parseFloat(a['subject_id']) < parseFloat(b['subject_id']))
        return -1
      if (parseFloat(a['subject_id']) > parseFloat(b['subject_id']))
        return 1
      return 0
    data.sort(compare) #order subjects to match subject id... could be better

    for d,i in data
      subject = d['subject']
      sid = parseInt(d['subject_id'])
      switch sid
        when -1 then labels[i] = "Week"
        when -2 then labels[i] = "Month"
        when -3 then labels[i] = "Quarter"
        when -4 then labels[i] = "Year"
        else labels[i] = subject
    
    axis.shapes.selectAll('text')
        .data(labels)
        .text((d) -> return d)
    

    wrapWidth = parseInt(unsubAxis.shapes.select('.tick text').style('width')) + 100

    axis.shapes.selectAll('text')
        .call(wrap, wrapWidth)

  showEveryNthGridline = (axis, oneInEvery) ->
    if oneInEvery > 0 && axis.shapes.length > 0
      del = 0
      axis.shapes.selectAll("text").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.shapes.selectAll("line").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.gridlineShapes.selectAll("line").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;

  chartDuration = 800 #Length (ms) for chart to fully render

  renderChart = (init) ->
    ocaChart.draw(chartDuration);
    makeLegendInteractive(ocaChart,unsubChart,ocaLegend)
    unsubChart.draw(chartDuration);
    unsubAxis.titleShape.remove()
    ocaMeasure.titleShape.remove()
    unsubAxis.titleShape.remove()
    unsubMeasure.titleShape.remove()
    replaceUnsubAxisLabels(Data.mailingDetailSummary, unsubAxis)
    removeOriginPercent()

  addAggregateData()
  ocaData = buildMailingListSummaryChartData(Data.mailingDetailSummary, ["Actions", "Clicks", "Opens"])
  ocaSvg = dimple.newSvg("#MailingDetailEventChart", "100%", "100%")
  ocaChart = new dimple.chart(ocaSvg, ocaData);

  ocaAxis  = ocaChart.addCategoryAxis("x", ["Subject ID", "Event"]);
  ocaAxis.hidden = true

  ocaMeasure = ocaChart.addMeasureAxis("y", "Percent");
  ocaMeasure.tickFormat = ".2%"

  window.ocaSeries  = ocaChart.addSeries("Event", dimple.plot.bar, [ocaAxis, ocaMeasure]);
  ocaSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, ocaSvg, ocaSeries))
  ocaSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, ocaSvg, ocaSeries))

  ocaSeries.addOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"], true)

  ocaLegend = ocaChart.addLegend('50%', 5, '40%', 20, "right", ocaSeries)

  ocaChart.assignColor("Opens", "#8E44AD", "#8E44AD", 1);
  ocaChart.assignColor("Clicks", "#2C3E50", "#2C3E50", 1);
  ocaChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
  ocaChart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  ocaChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  ocaChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);
  ocaChart.setMargins("10%", "60px", "10%", "0");
  
  unsubData = buildMailingListSummaryChartData(Data.mailingDetailSummary, ["Unsubs", "Unsub_Bounces", "Unsub_Complaints"])
  unsubSvg = dimple.newSvg("#MailingDetailUnsubChart", "100%", "100%")
  unsubChart = new dimple.chart(unsubSvg, unsubData);

  unsubAxis = unsubChart.addCategoryAxis("x", ["Subject ID", "Event"]);
  unsubAxis.addOrderRule('Started At')
  unsubAxis.addGroupOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"])

  unsubMeasure = unsubChart.addMeasureAxis("y", "Percent");
  unsubMeasure.tickFormat = ".2%"

  window.unsubSeries = unsubChart.addSeries("Event", dimple.plot.bar, [unsubAxis, unsubMeasure]);
  unsubSeries.addOrderRule(["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"], true)
  unsubSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, unsubSvg, unsubSeries))
  unsubSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, unsubSvg, unsubSeries))

  unsubChart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  unsubChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  unsubChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);

  unsubChart.setMargins("10%", "0", "10%", "60px");

  renderChart(true)
  showEveryNthGridline(unsubMeasure, 4)

  window.onresize = () ->
    renderChart(false)


$ ->
  if Page == 'mailingDetail'
    initDetailChart()
