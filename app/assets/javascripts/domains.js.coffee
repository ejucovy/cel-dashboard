# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#
# Mailing Domains Time Series Chart
#
mailingDomainsTimeSeriesChart = () ->

  createEventLegend = (chart) ->
    #access svg from dimple chart and roll our own legend, since Dimple doesn't really
    #support what we are trying to do with dashed lines.

    filterValues = dimple.getUniqueValues(chart.data, "event")
    window.legend = chart.svg.selectAll('.legend')
                          .data(filterValues)
                        .enter().append('g')
                          .attr('class', 'legend')
                          .attr('transform', (d, i) ->
                            return "translate(" + ((i * 55) + 80) + ",10)"
                          )

    legend.append('line')
          .attr('x1', 10)
          .attr('x2', 23)
          .attr('y1', 0)
          .attr('y2', 0)
          .style('stroke-width', '3')
          .style('stroke-dasharray', (d) -> 
            if d == "opens"
              return 0
            else if d == "clicks"
              return "6,2"
            else if d == "actions"
              return "2,3"
            else if d == "unsubs"
              return "4,4"
            else if d == "complaints"
              return "7,5" # TODO: needs some better styling
            else if d == "bounces"
              return "9,6" # TODO: needs some better styling
          )
          .style('stroke', '#BBD')
          .style('shape-rendering', 'crispedges')

    legend.append('text')
      .attr('x', 42)
      .attr('y', 0)
      .attr('dy', '.35em')
      .attr('height', 10)
      .style('font-size', '10px')
      .style('font-family', 'sans-serif')
      .style('text-anchor', 'middle')
      .style('cursor', 'pointer')
      .style('shape-rendering', 'crispedges')
      .style("opacity", (d) ->
          filterValues = ["opens"]
          return 0.3 if d != "opens"
        )
      .text((d) -> return d)

    legend.selectAll("line,text")
                 .on "click", (e) -> 
                  hide = false
                  newFilters = [] #metrics that will be charted at the end
                  metricClicked = e
                  svg = d3.select(@)

                  #loop through values that are visible, see if the one that clicked needs to be hidden
                  filterValues.forEach (f) ->
                    if (f == metricClicked) 
                      hide = true #if hide, the item was already visible
                    else
                      newFilters.push(f)

                  if hide 
                    svg.style("opacity", 0.3) 
                  else 
                    newFilters.push(metricClicked)
                    svg.style("opacity", 0.8)

                  filterValues = newFilters
                  chart.data = dimple.filterData(mailingDomainsTsChartData, "event", filterValues)
                  chart.draw(200)
                  makeDomainLegendInteractive(mailingDomainsTsDimpleChart, mailingDomainsTsDimpleDomainLegend)

  makeDomainLegendInteractive = (chart, legend) ->
    #And then we want to extend the domain legend to trigger checkboxes,
    #creating a feeling of interactivity with the SVG.
    #has to be applied any time we redraw the chart, since we don't 
    #want to orphan the legend from Dimple's dynamic functionality.
    legend.shapes.selectAll('text, rect')
                 .style("cursor", "pointer")
                 .on "click", (e) ->
                  did = e.aggField.slice(1,2)[0]
                  $('.domain-id[value=' + did + ']').trigger('click')

  createTsDimpleChart = () ->
    if typeof(mailingDomainsTsDimpleChart) == "undefined"
      svg = dimple.newSvg("#MailingDomainsTimeSeriesChart", '100%', '100%');
      #look at data
      window.mailingDomainsTsDimpleChart = new dimple.chart(svg, window.mailingDomainsTsChartData);
    if mailingDomainsTsDimpleChart.axes.length == 0
      dateAxis = mailingDomainsTsDimpleChart.addTimeAxis("x", "period")
      dateAxis.tickFormat = "%I:%M%p"
      dateAxis.timePeriod = d3.time.hours
      dateAxis.timeInterval = 1
      eventAxis   = mailingDomainsTsDimpleChart.addMeasureAxis("y", "count")
      domainSeries = mailingDomainsTsDimpleChart.addSeries(["event", "domain_id", "domain"], dimple.plot.line, [dateAxis, eventAxis])
      window.mailingDomainsTsDimpleDomainLegend = mailingDomainsTsDimpleChart.addLegend('30%', 5, '70%', 20, "right", domainSeries)

  updateMailingDomainsTsChartData = (data) ->
    window.mailingDomainsTsChartData = window.mailingDomainsTsChartData.concat(data)

  swapOtherLegendLabel = () ->
      # cheap hack for replacing "other" w/ "everything else" until we need to make a new 
      # client-specific migration and can properly update in DB
      $('text.dimple-other').text($('.other-cell').text()) 

  renderChart = (init) ->
    if init
      #And one hack is replaced with another.  Because legends are based on data existing,
      #it isn't a trivial task to create an interactive legend that is essentially the 
      #opposite of this - http://dimplejs.org/advanced_examples_viewer.html?id=advanced_interactive_legends

      #We create a dummy dataset that is as lightweight as possible, draw it, 
      #orphan the legend from the chart and add interactivity to it, and then redraw with the 
      #appropriate data.  If everything goes according to plan, this should be good.

      dummyDataForLegend = [{
        event:"opens"
        period:0
      },
      {
        event:"clicks"
        period:0
      },
      {
        event:"actions"
        period:0
      },
      {
        event:"unsubs"
        period:0
      },
      {
        event:"complaints"
        period:0
      },
      {
        event:"bounces"
        period:0
      }
      ]

      mailingDomainsTsDimpleChart.data = dummyDataForLegend
      mailingDomainsTsDimpleChart.draw()
      createEventLegend(mailingDomainsTsDimpleChart)
      mailingDomainsTsDimpleChart.data = dimple.filterData(mailingDomainsTsChartData, "event", "opens")
      mailingDomainsTsDimpleChart.draw()
      makeDomainLegendInteractive(mailingDomainsTsDimpleChart, mailingDomainsTsDimpleDomainLegend)
    else if window.mailingDomainsTsChartData.length > 0
      mailingDomainsTsDimpleChart.data = mailingDomainsTsChartData
      mailingDomainsTsDimpleChart.draw()
      makeDomainLegendInteractive(mailingDomainsTsDimpleChart, mailingDomainsTsDimpleDomainLegend)
    swapOtherLegendLabel()
  
  # TODO: Set updating icon before the post(), and unset after the post()
  getTsData = (domainIds) ->
    $.post("/email/time_series", {mailing_id: mailingId, domain_ids: domainIds.join(','), domain_keys: true }).done (data) ->
      for key in Object.keys(data)
        d['period'] = moment(d['period']) for d in data[key] #convert to more x-browser friendly format
        window.mailingDomainsTsData[key] = data[key]
        updateMailingDomainsTsChartData(data[key])
      createTsDimpleChart()
      renderChart(true)
      

  checkedDomainIds = for cb in $('.graph-ts.domain-id:checked')
    cb.value

  window.mailingDomainsTsChartData.length = 0
  mailingDomainsTsChartDataNeeded = []

  for d in checkedDomainIds
    if mailingDomainsTsData[d] == undefined
        mailingDomainsTsChartDataNeeded.push(d)
    else
      updateMailingDomainsTsChartData(mailingDomainsTsData[d])

  createTsDimpleChart()

  if mailingDomainsTsChartDataNeeded.length > 0
    getTsData(mailingDomainsTsChartDataNeeded)
  else
    renderChart(true)

  window.onresize = () ->
      renderChart()

setClickHandlers = ->
  $('.graph-ts.domain-id').on 'click', ->
    mailingDomainsTimeSeriesChart()

$ ->
  window.mailingDomainsTsData = {}
  window.mailingDomainsTsChartData = []
  window.mailingDomainsTsDimpleChart

  setClickHandlers()
  mailingDomainsTimeSeriesChart()