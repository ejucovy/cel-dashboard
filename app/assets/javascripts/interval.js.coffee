showEveryNthGridline = (axis, oneInEvery) ->
  if oneInEvery > 0 && axis.shapes.length > 0
    del = 0
    axis.shapes.selectAll("text").each (d) ->
      this.remove() if del % oneInEvery != 0; del += 1;
    del = 0
    axis.shapes.selectAll("line").each (d) ->
      this.remove() if del % oneInEvery != 0; del += 1;
    del = 0
    if axis.gridlineShapes is not null
      axis.gridlineShapes.selectAll("line").each (d) ->
        this.remove() if del % oneInEvery != 0; del += 1;

formatAxisLabels = (axis) ->
  axis.shapes.selectAll('text')
             .text (d, i) ->
                format = d3.time.format("%I:%M%p")
                formatted_label = format(new Date(d))
                return formatted_label


#TODO: Needs to feel smoother
#TODO: Known bug: when a mailing is checked and metrics are clicked, the mailing charts with all metrics
#Quickest fix would be to reset metric settings.
#More complex, but better fix, would be to add a check to where we separate returned data into 
#the chart and Data.hiddenMetrics, based on Data.hiddenMetricKeys
makeLegendInteractive = (chart, legend) ->
  #remove legend from chart, so that it doesn't update (i.e. remove metrics) when the chart updates
  chart.legends = []
  #grab all of our metric names to start
  filterValues = dimple.getUniqueValues(chart.data, "Event");

  legend.shapes.selectAll("rect").on "click", (e) -> 
    hide = false
    newFilters = [] #metrics that will be charted at the end
    metricClicked = e.aggField.slice(-1)[0]

    #TODO: Improve data structure for hidden data and associated metrics (for easy indexing)
    if typeof Data.hiddenMetricKeys == "undefined"
      Data.hiddenMetricKeys = [] 

    #loop through values that are visible, see if the one that clicked needs to be hidden
    filterValues.forEach (f) ->
      if (f == metricClicked) 
        hide = true #if hide, the item was already visible
        Data.hiddenMetricKeys.push(f)
        if typeof Data.hiddenMetrics == "undefined"
          Data.hiddenMetrics = dimple.filterData(chart.data, "Event", metricClicked)
        else 
          Data.hiddenMetrics = Data.hiddenMetrics.concat(dimple.filterData(chart.data, "Event", metricClicked))
      else
        newFilters.push(f)
    
    if hide 
      d3.select(this).style("opacity", 0.2) 
    else 
      d3.select(this).style("opacity", 0.8)
      if $.inArray(metricClicked, Data.hiddenMetricKeys) != -1
        Data.hiddenMetricKeys.splice($.inArray(metricClicked,Data.hiddenMetricKeys), 1)
      newFilters.push(metricClicked)
          
    filterValues = newFilters

    if hide          
      chart.data = dimple.filterData(chart.data, "Event", filterValues)
    else
      hiddenData = dimple.filterData(Data.hiddenMetrics, "Event", metricClicked)
      chart.data = chart.data.concat(hiddenData)
      Data.hiddenMetrics = dimple.filterData(Data.hiddenMetrics, "Event", Data.hiddenMetricKeys)
    chart.draw(800)
       



buildMailingIntervalChartData = (data, eventsWanted) ->
  chartData = []
  summedData = [
    {'type':'Opens', 'count':0}, 
    {'type':'Clicks', 'count':0}, 
    {'type':'Actions', 'count':0}, 
    {'type':'Unsubs', 'count': 0}, 
    {'type':'Unsub_Bounces', 'count': 0}, 
    {'type':'Unsub_Complaints', 'count': 0}, 
  ]

  for row in data
    for event in summedData

      if eventsWanted.indexOf(event.type) > -1
        count = parseInt(row[event.type.toLowerCase()])
      else
        count = 0

      event.count  = event.count + count
      d = {}
      d['Event'] = event.type
      d['Count'] = event.count
      if event.count == 0
        d['Count'] = null #TODO: Figure out why setting this to null fixes the bug in which the Dimple chart displaying zero sum columns strangely.
      else
        d['Count'] = event.count
      d['Period'] = moment(row['period'])
      chartData.push(d)
  return chartData

initMailingIntervalChart = () ->

  data = buildMailingIntervalChartData(Data.intervalTimeSeries, ["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Complaints", "Unsub_Bounces"])

  #build our chart svg nodes
  svg = dimple.newSvg("#MailingIntervalChart", "100%", "100%")
  
  #add the charts with chartData binded
  window.chart = new dimple.chart(svg, data)

  #add our axes - using composite axes
  Axes = {}
  Axes.period = chart.addCategoryAxis('x', 'Period')
  Axes.count = chart.addMeasureAxis('y', 'Count')


  #plot our series
  Series = {}
  Series.events = chart.addSeries("Event", dimple.plot.area) 
  Series.events.stacked = false;

  #TODO: Inform Dimple of bug
  #we have to order the event counts in ascending order, or else the chart plots 
  #opposite to what is drawn in the legend.  only happens if stacked is false.
  Series.events.addOrderRule('Count', false)

  chart.setMargins("10%", "40px", "10%", "40px");
  chart.assignColor("Opens", "#8E44AD", "#8E44AD", 1);
  chart.assignColor("Clicks", "#2C3E50", "#2C3E50", 1);
  chart.assignColor("Actions", "#27AE60", "#27AE60", 1);
  chart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  chart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  chart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);

  legend = chart.addLegend('50%', 5, '40%', 20, "right", Series.events)
  window.onresize = () ->
    chart.draw(0, true) #true means we don't update data
    formatAxisLabels(Axes.period)

  chart.draw()
  showEveryNthGridline(Axes.period, 4)
  formatAxisLabels(Axes.period)
  makeLegendInteractive(chart, legend)

  

$ ->
  if window.Page == 'mailingInterval'
    initMailingIntervalChart()