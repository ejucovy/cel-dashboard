tsParse = (ts) ->
  [d, t] = ts.split(' ')
  [y, mon, d] = d.split('-')
  [h, min, s] = t.split(':')
  dt = new Date(y, mon, d, h, min, s)
  return dt

initUserSourceChart = () ->

  buildTooltip = (e, svg, series) ->
    #config so you don't have to read my silly code!
    config = {
      borderWidth: 5 #pixels
      borderColor: e.selectedShape.attr("fill")
      bgColor: '#FFF'
      font: 'sans-serif'
      fontColor: '#222'
      fontSize: '11px'
      width: 150
      height: 55
    }

    if $.inArray(e.seriesValue[0], ["Unsubs", "Unsub_Complaints", "Unsub_Bounces"]) >= 0
      unsub = true

    #Get the properties of the selected shape
    cx = parseFloat(e.selectedShape.attr("x"))
    cy = parseFloat(e.selectedShape.attr("y"))
    height = parseFloat(e.selectedShape.attr("height")) #don't get this.

    #Set the size and position of the tooltip
    x = if cx + config.width + 10 < svg.attr("width") then cx + 10 else cx - config.width - 20
    y = if cy - config.height / 2 < 0 then 25 else 150

    #Create a group for the tooltip objects
    #TODO: get out of global namespace icky!
    tooltip = series.chart._tooltipGroup = svg.append("g").attr("class", "tooltip-wrapper")

    #Add a rectangle surrounding the text pop
    tooltip.append("rect")  
    .attr("x", x + 140)
    .attr("y", y - 5)
    .attr("width", config.width)
    .attr("height", config.height)
    .attr("rx", 5)
    .attr("ry", 5)
    .style("fill", config.bgColor)
    .style("stroke", config.borderColor)
    .style("stroke-width", config.borderWidth);

    switch parseInt(e.xValue)
      when -4 then mid = "Year"
      when -3 then mid = "Quarter"
      when -2 then mid = "Month"
      when -1 then mid = "Week"
      else mid = e.xValue
    
    if unsub
      # TODO: i believe there is a bug in dimple that is preventing the eventArgs object from passing the height of the 
      # value as it's yValue when the height in negative (i.e. for unsub, complaints, and bounces).  will follow up/.
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + '-' + parseInt(e.selectedShape[0][0].attributes.height.nodeValue)/100.toFixed(2) + '%', 'Mailing ID: ' + mid]
    else
      window.text = ['Metric: ' + e.seriesValue[0], 'Rate: ' + (e.yValue * 100).toFixed(2) + '%', 'Mailing ID: ' + mid]
    
    tooltip.append('text')
           .selectAll('tspan').data(text)
           .enter()
           .append('tspan')
           .attr('x', x + 152)
           .attr('y', (d,i) -> return y + ((i + 1) * 13))
           .text((d) -> return d)
           .style("font-family", config.font)
           .style("font-size", config.fontSize)
           .style("color", config.fontColor)

    dropDest = series._dropLineOrigin()
    offset = if series._isStacked() then 1 else width / 2

    if not series.y._hasCategories() and dropDest.x != null 
      series.chart._tooltipGroup.append("line")
           .attr("x1", (if cx < dropDest.x then cx + width else cx))
           .attr("y1", (if unsub then height else cy))
           .attr("x2", (if cx < dropDest.x then cx + width else cx))
           .attr("y2", (if unsub then height else cy))
           .style("fill", "none").style("stroke", e.selectedShape.attr("fill"))
           .style("stroke-width", 2).style("stroke-dasharray", ("3, 3"))
           .style("opacity", e.selectedShape.attr("opacity"))
           .transition()
           .delay(750 / 2)
           .duration(750 / 2)
           .ease("linear")
           .attr("x2", (if x < dropDest.x then dropDest.x - 1 else dropDest.x + 1))
        #Added 1px offset to cater for svg issue where a transparent
        #group overlapping a line can sometimes hide it in some browsers
        #Issue #10
  
  chartDuration = 800 #Length (ms) for chart to fully render

  removeTooltip = (e, svg, series) ->
    #TODO: cache objects in DOM if they've been instantiated so we only get
    #one instance of a tooltip rather than rebuild?  
    if series.chart._tooltipGroup != null
      series.chart._tooltipGroup.remove()

  buildMailingListSummaryChartData = (data, eventsWanted) ->
    chartData = []
    for row in data
      for event in ["Opens", "Clicks", "Actions", "Unsubs", "Unsub_Bounces", "Unsub_Complaints"]
        if eventsWanted.indexOf(event) > -1
          count = parseInt(row[event.toLowerCase()])
          pct = parseInt(row[event.toLowerCase()]) / parseInt(row['sent'])
          pct = -pct if $.inArray(event,  ['Unsubs', 'Unsub_Bounces', 'Unsub_Complaints']) >= 0
        else
          count = 0
          pct = 0

        # for now, I'm hiding the aggregate.  I will update to show aggregate when # of subjects > 1
        if parseInt(row['subject_id']) != 0
          d = {}
          d['Mailing ID'] = row['mailing_id']
          d['Source']     = row['source']
          d["Event"]      = event
          d['Percent']    = pct
          d['Count']      = count
          d['Sent']       = row['sent']
          chartData.push(d)                         
    return chartData

  replaceEventAxisLabels = (data, axis) ->
      labels = []
      for d in data
        labels[d['subject_id']] = d['subject'] 
      axis.shapes.selectAll('text').text (d, i) ->
        new_label = ''
        if labels[d]
          new_label = labels[d]
        return new_label

  showEveryNthGridline = (axis, oneInEvery) ->
    if oneInEvery > 0 && axis.shapes.length > 0
      del = 0
      axis.shapes.selectAll("text").each (d) ->
        @.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.shapes.selectAll("line").each (d) ->
        @.remove() if del % oneInEvery != 0; del += 1;
      del = 0
      axis.gridlineShapes.selectAll("line").each (d) ->
        @.remove() if del % oneInEvery != 0; del += 1;

  # after draw, use text() to select exact matches for 0 values
  # on measure charts.  need to make this less generalizable since
  # it really only applies to the condition in which we have
  # two charts stacked together.
  removeOriginPercent = () ->
    text = "0.00%"
    text2 = "0"
    zeroTick = $('.tick').filter () ->
       return $(@).text() == text || $(@).text() == text2
    zeroTick.remove()

  addSendCounts = (dataChanged) ->
     ocaSeries3.afterDraw = (shape, data) ->
      # TODO: Figure out how to redraw position when browser is resized.
      # TODO: Make toggleable.

      s = d3.select(shape)
      circle = { 'cx': s.attr('cx'), 'cy': s.attr('cy') - 12 }

      # its grouping the summary objects together by event and incorrectly
      # adding the sends up, rather than splitting as they would if we were
      # using the data the `right` way.  so let's divide send count by 4. 
      if not dataChanged
        ocaSvg.append("text")
              .attr("x", circle.cx)
              .attr("y", circle.cy)
              .attr("class", "send-count")
              .text(d3.format(",")(data.yValue/4)) 

  fixBlankSource = (charts) ->
    for chart in charts
      chart.data.filter (d) ->
        if d['Source'] == "" 
          d['Source'] = "unknown" 

  renderChart = (dataChanged) ->
    fixBlankSource([ocaChart, unsubChart])
    addSendCounts(dataChanged) 
    ocaChart.draw(0, dataChanged);
    unsubChart.draw(0, dataChanged);
    unsubMeasure.titleShape.remove()
    showEveryNthGridline(unsubMeasure, 4)
    removeOriginPercent([ocaMeasure, ocaMeasure2])

  ocaData = buildMailingListSummaryChartData(mailingUserSourceSummaryData, ["Opens", "Clicks", "Actions"])
  ocaSvg = dimple.newSvg("#MailingUserSourceEventChart", "100%", "100%")
  ocaChart = new dimple.chart(ocaSvg, ocaData);

  ocaAxis  = ocaChart.addCategoryAxis("x", ["Source", "Event"]);
  ocaAxis.addOrderRule('Sent', true)
  ocaAxis.hidden = true

  ocaMeasure = ocaChart.addMeasureAxis("y", "Percent");
  ocaMeasure.tickFormat = ".2%"

  ocaSeries  = ocaChart.addSeries("Event", dimple.plot.bar, [ocaAxis, ocaMeasure]);
  ocaSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, ocaSvg, ocaSeries))
  ocaSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, ocaSvg, ocaSeries))
  ocaAxis2 = ocaChart.addCategoryAxis("x", ["Source"]);
  ocaAxis2.hidden = true
  ocaMeasure2 = ocaChart.addMeasureAxis("y", "Sent");

  # plotting an empty line, on which we will overlay a bubble chart 
  ocaSeries2 = ocaChart.addSeries(null, dimple.plot.line, [ocaAxis2, ocaMeasure2]);
  ocaSeries3 = ocaChart.addSeries("Sent", dimple.plot.bubble, [ocaAxis2, ocaMeasure2]);

  ocaChart.assignColor("Opens",   "#8E44AD", "#8E44AD", 1);
  ocaChart.assignColor("Clicks",  "#2C3E50", "#2C3E50", 1);
  ocaChart.assignColor("Actions", "#27AE60", "#27AE60", 1);
  ocaChart.assignColor("Unsubs",  "#C0392B", "#C0392B", 1);
  ocaChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  ocaChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);

  unsubData = buildMailingListSummaryChartData(mailingUserSourceSummaryData, ["Unsubs", "Unsub_Bounces", "Unsub_Complaints"])
  unsubSvg = dimple.newSvg("#MailingUserSourceUnsubChart", "100%", "100%")
  unsubChart = new dimple.chart(unsubSvg, unsubData);

  unsubAxis = unsubChart.addCategoryAxis("x", ["Source", "Event"]);
  unsubAxis.addOrderRule('Sent', true)
  
  unsubMeasure = unsubChart.addMeasureAxis("y", "Percent");
  unsubMeasure.tickFormat = ".2%"

  unsubSeries = unsubChart.addSeries("Event", dimple.plot.bar, [unsubAxis, unsubMeasure]);
  unsubSeries.addEventHandler('mouseover', (e) -> buildTooltip(e, unsubSvg, unsubSeries))
  unsubSeries.addEventHandler('mouseleave', (e) -> removeTooltip(e, unsubSvg, unsubSeries))

  unsubChart.assignColor("Unsubs", "#C0392B", "#C0392B", 1);
  unsubChart.assignColor("Unsub_Complaints", "#FFA500", "#FFA500", 1);
  unsubChart.assignColor("Unsub_Bounces", "#CCCC00", "#CCCC00", 1);

  ocaChart.setMargins("10%", "60px", "10%", "0");
  unsubChart.setMargins("10%", "0", "10%", "60px");

  ocaChart.addLegend('30%', 5, '70%', 20, "right", ocaSeries)
  renderChart(false)

  window.onresize = () ->
    renderChart(true)

$ ->
  if mailingPageType == 'userSource'
    initUserSourceChart()
