initOverviewChart = () ->

  createTooltipDiv = () ->
    $("<div id='tooltip'></div>").css({
      position: "absolute",
      display: "none",
      border: "3px solid #fdd",
      padding: "2px",
      "background-color": "#fee",
      opacity: 0.80
    }).appendTo("body")

  tooltip = (event, pos, item) ->
    if (item)
      dt = new Date(parseInt(item.datapoint[0].toFixed(2)))
      val = parseInt(item.datapoint[1].toFixed(2))

      $("#tooltip").html(dt.toDateString() + '<br />' + item.series.label + ": " + val)
        .css({top: item.pageY - 40, left: item.pageX + 5})
        .fadeIn(200)
    else 
      $("#tooltip").hide()

  calcBarWidth = () ->
    min = overviewData[0]['period']
    max = 0
    for e in overviewData
      min = Math.min(min, e['period'])
      max = Math.max(max, e['period'])
    # The '5' in the next line was determiend by trial and error...
    (max - min) / (overviewData.length * 5)

  unsubMinimum = () ->
    min = 0
    if $('.legendColorBox#Unsubs').find('div div').css('opacity') == "1"
      for e in overviewData
        min = Math.min(min, -e['unsubs'])
    min

  buildChartData = (series) ->

    pluckData = (col) ->
      col = col.toLowerCase().replace(' ', '_')
      d = []
      for e in overviewData
        if col == 'unsubs'
          d.push( [ e['period'], -e[col] ] )
        else
          d.push( [ e['period'], e[col] ] )
      d

    ds = new Array()
    if series.indexOf('actions') >= 0
      ds.push({
        bars: { order: 1, show: true, fill: true, fillColor: 'rgba(39, 174, 96, 1.0)' },
        lines: { show: false}
        data: pluckData('actions')
        label: 'Actions',
        color: 'rgba(39, 174, 96, 1.0)',
        highlightColor: 'rgba(39, 174, 96, 0.2)',
        yaxis: 1,
        type: 'bar'
      })
    if series.indexOf('new_members') >= 0
      ds.push({
        bars: { order: 2, show: true, fill: true, fillColor: 'rgba(44, 45, 80, 1.0)' },
        data: pluckData('new_members')
        label: 'New Members',
        color: 'rgba(44, 45, 80, 1.0)',
        fillColor: 'rgba(44, 45, 80, 0.2)',
        yaxis: 1,
        type: 'bar'
      })
    if series.indexOf('unsubs') >= 0
      ds.push({ 
        bars: { order: 3, show: true, fill: true, fillColor: 'rgba(192, 57, 43, 1.0)' },
        data: pluckData('unsubs')
        label: 'Unsubs',
        color: 'rgba(192, 57, 43, 1.0)',
        fillColor: 'rgba(192, 57, 43, 0.2)',
        yaxis: 1,
        type: 'bar'
      })
    if series.indexOf('mailable') >= 0
      ds.push({ 
        lines: { show: true, fill: false },
        data:  pluckData('mailable'),
        label: 'Mailable',
        color: 'grey',
        yaxis: 2,
        type: 'line'
      })
    ds

  toggleSeries = (eo) ->
    if $(this).attr('class') == 'legendColorBox'
      clickedSeries = $(this).next('td').html().trim()
      colorBox      = $(this)
    else 
      clickedSeries = $(this).html().trim()
      colorBox      = $(this).prev('td')

    # Find the clicked series and toggle its color
    seriesToChart = []
    $('.legendLabel').each (index) ->
      s = $(this).html().trim()
      if s == clickedSeries
        if colorBox.find('div div').css('opacity') == "0"
          colorBox.find('div div').css('opacity', 1)
        else
          colorBox.find('div div').css('opacity', 0)
      # Determine the series to be displayed
      if $('.legendColorBox#' + s.replace(' ', '')).find('div div').css('opacity') == "1"
        seriesToChart.push(s.toLowerCase().replace(' ', '_')) 

    data = buildChartData(seriesToChart)
    overviewChart.getOptions().yaxes[0].min = unsubMinimum()
    overviewChart.setData(data)
    overviewChart.setupGrid()
    overviewChart.draw()

  options = () ->
    {
      grid: { 
        hoverable: true, 
      },
      axisLabels: { show: true },
      xaxes: [ { mode: "time", timezone: 'browser', timeformat: "%m-%d-%y" } ],
      yaxes: [ 
        { min: unsubMinimum(), position: 'left', axisLabel: 'Events' }, 
        { alignTicksWithAxis: 1, position: 'right' , axisLabel: 'Mailable' }
      ],
      legend: { show: false },
      series: {
        bars: {
          barWidth: calcBarWidth(),
          lineWidth: 2,
          align: "left"
        }
      },
      xaxis: { tickLength: 0},
    }

  # Set up the AK List selection and time frame btn
  $('form#listHealth select').chosen({width: '100%'})
  $('form#listHealth select').on 'chosen:hiding_dropdown', () =>
    $('form#listHealth').submit()
  $('form#listHealth input, form#listHealth select').change () ->
    $('form#listHealth').submit()

  # Set up the chart
  createTooltipDiv()
  data = buildChartData(['actions', 'new_members', 'unsubs', 'mailable'])
  window.overviewChart = $.plot($("#overviewChart"), data, options());
  $("#overviewChart").bind("plothover", tooltip);
  $('td.legendColorBox, td.legendLabel').click(toggleSeries)


toggleSubjects = (obj) ->
  mid = $(this).parent().data('mailing-id')
  $('tr.email-agg[data-mailing-id=' + mid + '] td.show-subjects').find('span').toggleClass('glyphicon-plus glyphicon-minus');
  $('tr.email-subject[data-mailing-id=' + mid + ']').toggleClass('show-subject')

setClickHandlers = ->
  # Mailing List: subjects hide/show
  $('tr.email-agg td.show-subjects').on 'click', ->
    toggleSubjects.apply(@)

toggleSubjects = (obj) ->
  mid = $(this).parent().data('mailing-id')
  $('tr.email-agg[data-mailing-id=' + mid + '] td.show-subjects').find('span').toggleClass('glyphicon-plus glyphicon-minus');
  $('tr.email-subject[data-mailing-id=' + mid + ']').toggleClass('show-subject')

$ ->
  if pagePageType == 'overview'
    initOverviewChart()
    setClickHandlers()


