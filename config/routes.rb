CelCenter::Application.routes.draw do
  root :to => "home#index", :via => [:get, :post]
  #devise_for :users, :controllers => {:registrations => "registrations"}
  #resources :users

  #new code begin
  devise_for :users, :skip => [:registrations]
  devise_scope :user do
    get "signup",   :to => "accounts#new"
    get "signin",   :to => "devise/sessions#new"
    get "signout",  :to => "devise/sessions#destroy"
    get "cancel_user_registration", :to => "devise/registrations#cancel"
    post "user_registration",       :to => "users#create"
    get "new_user_registration",    :to => "accounts#new"
    get "edit_user_registration",   :to => "users#edit"
  end
  resources :users
  resources :accounts
  # new code end

  namespace :admin do
    resources :clients, controller: 'clients'
  end

  post '/admin/set_current_client', to: 'home#set_current_client'

  match '/emails' => 'mailings#index', :via => [:get, :post]

  match '/emails/comparison' => 'mailings#comparison', :via => [:get, :post]

  get '/email/:mailing_id/', to: redirect('/email/%{mailing_id}/detail')
  get '/email/:mailing_id/detail', to: 'mailings#detail'
  get '/email/:mailing_id/interval', to: 'mailings#interval'
  get '/email/:mailing_id/email_domains', to: 'mailings#email_domains'
  get '/email/:mailing_id/user_sources', to: 'mailings#user_sources'
  get '/email/:mailing_id/links', to: 'mailings#links'

  post '/email/time_series', to: 'mailings#time_series'

  match '/pages' => 'pages#index', :via => [:get, :post]
  get '/page/:page_id/detail',         to: 'pages#detail'
  get '/page/:page_id/action_sources', to: 'pages#action_sources'
  get '/page/:page_id/user_sources',   to: 'pages#user_sources'

  post '/page/time_series', to: 'pages#time_series'

  match '/typeahead' => 'typeahead#index', :via => [:get, :post]

  get '/pv/1.gif' => 'page_views#record_page_view'
end
