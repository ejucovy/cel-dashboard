#  set :bundle_gemfile,  "Gemfile"
#  set :bundle_dir,      File.join(fetch(:shared_path), 'bundle')
#  set :bundle_flags,    "--deployment --quiet"
#  set :bundle_without,  [:development, :test]
#  set :bundle_cmd,      "bundle" # e.g. "/opt/ruby/bin/bundle"
#  set :bundle_roles,    {:except => {:no_release => true}} # e.g. [:app, :batch]
set :default_environment, {
 'LANG'   => "en_US.UTF-8",
 'LC_ALL' => "en_US.UTF-8"
}
set :default_shell, "bash -l"

require "bundler/capistrano"
require 'capistrano/sidekiq'
load 'deploy/assets'

set :application, "cc"

set :use_sudo, false
default_run_options[:pty] = true

set :keep_releases, 10
after "deploy:update", "deploy:cleanup"


# Repo info
set :repository, "git@bitbucket.org:engagementlab/cel-dashboard.git"
set :scm, "git"

ssh_options = {
  forward_agent: true,
  auth_methods: %w(publickey),
  keys_only: true,
}

set :branch, "master"
set :deploy_via, :remote_cache

# Deploy target info
set :deploy_to, "/home/admin/cc"
role :web, "dash.example.org"
role :app, "dash.example.org"
role :db,  "dash.example.org", :primary => true

# Sidekiq
set(:sidekiq_cmd) { "#{current_path}/bin/cc_run.sh sidekiq" }
set(:sidekiqctl_cmd) { "#{current_path}/bin/cc_run.sh sidekiqctl" }
set(:sidekiq_timeout) { 10 }
set(:sidekiq_role) { :app }
set(:sidekiq_pid) { "#{current_path}/pids/cc_sidekiq.pid" }
set(:sidekiq_log) { "#{current_path}/log/cc_sidekiq.log" }
set(:sidekiq_processes) { 1 }


namespace :symlinks do
  desc "[internal] Updates the symlinks to config files (for the just deployed release)."
  task :set_links, :except => { :no_release => true } do
    [
      'database.yml', 'ak_database.yml','memcached.yml'
    ].each do |file|
      run "if [ -e #{shared_path}/config/#{file} ]; then ln -nfs #{shared_path}/config/#{file} #{release_path}/config/#{file}; fi"
    end
    [
      'log', 'pids'
    ].each do |file|
      run "if [ -e #{shared_path}/#{file} ]; then ln -nfs #{shared_path}/#{file} #{release_path}/#{file}; fi"
    end
  end
  after "deploy:finalize_update", "symlinks:set_links"

  desc "[internal] Creates a symlink to public/pv.jc from the latest version in public/assets"
  task :create_pv_js_link, :except => { :no_release => true } do
    run "cd #{release_path} && #{release_path}/bin/cc_run.sh ruby #{release_path}/bin/create_pv_js_link.rb"
  end
  before "deploy:create_symlink", "symlinks:create_pv_js_link"

end


set :rails_env, "production"
set :unicorn_binary, "#{shared_path}/bundle/ruby/2.0.0/bin/unicorn"
set :unicorn_config, "#{current_path}/config/unicorn-prod.rb"
set :unicorn_pid,    "#{shared_path}/pids/cc_app_master.pid"


namespace :deploy do

  namespace :unicorn do
    desc "Start unicorn (when they're not running)"
    task :start, :roles => :app, :except => { :no_release => true } do 
      run "cd #{current_path} && #{current_path}/bin/cc_run.sh #{unicorn_binary} -c #{unicorn_config} -E #{rails_env} -D"
    end

    desc "Stops unicorn immediately w/o waiting for active requests to complete"
    task :hard_stop, :roles => :app, :except => { :no_release => true } do 
      run "#{try_sudo} kill `cat #{unicorn_pid}`"
    end

    desc "Gracefully stops unicorn"
    task :stop, :roles => :app, :except => { :no_release => true } do
      run "#{try_sudo} kill -s QUIT `cat #{unicorn_pid}`"
    end

    desc "Gracefully restarts unicorn"
    task :restart, :roles => :app, :except => { :no_release => true } do
      run "#{try_sudo} kill -s USR2 `cat #{unicorn_pid}`"
    end

    desc "Performs a 'hard_stop' followed by a 'start'"
    task :hard_restart, :roles => :app, :except => { :no_release => true } do
      hard_stop
      start
    end
  end

  desc "Start services (when they're not running)"
  task :start, :roles => :app, :except => { :no_release => true } do 
    unicorn.start
  end

  desc "Stops services immediately w/o waiting for active requests to complete"
  task :hard_stop, :roles => :app, :except => { :no_release => true } do 
    unicorn.hard_stop
  end

  desc "Gracefully stops services"
  task :stop, :roles => :app, :except => { :no_release => true } do
    unicorn.stop
  end

  desc "Gracefully restarts services"
  task :restart, :roles => :app, :except => { :no_release => true } do
    unicorn.restart
end

  desc "Performs a 'hard_stop' followed by a 'start' for all services"
  task :hard_restart, :roles => :app, :except => { :no_release => true } do
    unicorn.hard_stop
    unicorn.start
  end

  task :echo_env do
    run "#{current_path}/bin/cc_run.sh env | sort"
  end
end


#
# Might be needed at some point if we get dup
# sidekiq processes on restart...
#
# namespace :sidekiq do
#   desc "Restart sidekiq"
#   task :restart, :roles => :app, :on_no_matching_servers => :continue do
#     run "sudo /usr/bin/monit restart sidekiq"
#   end
# end
