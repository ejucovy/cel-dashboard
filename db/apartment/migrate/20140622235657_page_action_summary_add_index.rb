class PageActionSummaryAddIndex < ActiveRecord::Migration
  def up
    execute(%q{DROP INDEX IF EXISTS index_page_action_summary_by_period_period })
    execute(%q{CREATE INDEX index_page_action_summary_by_period_period ON page_action_summary_by_period (period) })
  end

  def down
    execute(%q{DROP INDEX IF EXISTS index_page_action_summary_by_period_period })
  end
end
