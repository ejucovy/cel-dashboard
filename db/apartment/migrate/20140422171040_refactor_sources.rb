class RefactorSources < ActiveRecord::Migration

  def up
    rename_table :user_sources, :sources
    rename_column :sources, :user_source, :source
    rename_column :sources, :email_count, :user_count
    add_index :sources, [:action_count, :id, :source], order: {action_count: :desc} 

    rename_table :user_source_users, :source_users
    rename_column :source_users, :user_source_id, :source_id

    create_table :source_actions do |t|
      t.integer :source_id
      t.integer :action_id
    end
    add_index :source_actions, [:source_id, :action_id]
    add_index :source_actions, [:action_id, :source_id]

    execute(%q{DROP VIEW top_user_sources})

    execute(%q{CREATE VIEW top_user_sources AS 
                SELECT 0 AS id, 'other' AS source, 0 AS user_count
                UNION
                (SELECT id, source, user_count FROM sources ORDER BY user_count DESC LIMIT 49)})

    execute(%q{CREATE VIEW top_action_sources AS 
                SELECT 0 AS id, 'other' AS source, 0 AS action_count
                UNION
                (SELECT id, source, action_count FROM sources ORDER BY action_count DESC LIMIT 49)})
  end

  def down
    drop_table :source_actions

    rename_column :source_users, :source_id, :user_source_id
    rename_table :source_users, :user_source_users
   
    remove_index :sources, [:action_count, :id, :source]
    rename_column :sources, :user_count, :email_count
    rename_column :sources, :source,     :user_source

    rename_table :sources, :user_sources

    execute(%q{DROP VIEW top_user_sources})
    execute(%q{DROP VIEW top_action_sources})

    execute(%q{CREATE VIEW top_user_sources AS 
                SELECT 0 AS id, 'other' AS user_source, 0 AS email_count
                UNION
                (SELECT id, user_source, email_count FROM user_sources ORDER BY email_count DESC LIMIT 49)})


  end

end
