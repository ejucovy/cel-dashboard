class CreateMoreActionIndexs < ActiveRecord::Migration
  def up
    execute(%q{DROP INDEX IF EXISTS index_core_action_page_id })
    execute(%q{CREATE INDEX index_core_action_page_id ON core_action (page_id) })

    execute(%q{DROP INDEX IF EXISTS index_core_actionfield_parent_id_name })
    execute(%q{CREATE INDEX index_core_actionfield_parent_id_name ON core_actionfield (parent_id, name) })
  end

  def down
    execute(%q{DROP INDEX IF EXISTS index_core_action_page_id })
    execute(%q{DROP INDEX IF EXISTS index_core_actionfield_parent_id_name })
  end
end
