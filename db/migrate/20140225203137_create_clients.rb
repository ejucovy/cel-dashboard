class CreateClients < ActiveRecord::Migration
  def up
    create_table :clients, :force => true do |t|
      t.boolean  :active,         :null => false, :default => true
      t.boolean  :admin,          :null => false, :default => false
      t.string   :name,           :null => false
      t.string   :ak_schema_name, :null => false
      t.string   :cc_schema_name, :null => false
      t.integer  :master_user_id
      t.datetime :created_at,     :null => false
      t.datetime :updated_at,     :null => false
    end
    add_index :clients, :name, :unique => true
  end

  def down
    drop_table :clients
  end

end
