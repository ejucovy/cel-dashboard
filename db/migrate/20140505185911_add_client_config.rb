class AddClientConfig < ActiveRecord::Migration

  def up
    add_column :clients, :config, :json, :default => {}
  end
 
  def down
    remove_column :clients, :config
  end

end
