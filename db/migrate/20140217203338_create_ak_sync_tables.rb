class CreateAkSyncTables < ActiveRecord::Migration
  def up
    create_table :sync_tables do |t|
      t.string     :schema_name
      t.string     :table_name
      t.string     :pkey
      t.string     :pkey_type
      t.string     :sync_method, :default => 'ignore'  # reload, update_and_insert, subscription, no_pkey, _ignore, unknown
      t.boolean    :from_template, :default => false      
      t.integer    :src_col_cnt
      t.integer    :dst_col_cnt
      t.string     :err_msg
      t.timestamps
    end
    add_index :sync_tables, [:schema_name, :table_name], :unique => true

    create_table :sync_log do |t|
      t.string     :schema_name
      t.integer    :pid
      t.timestamp  :start_ts
      t.timestamp  :end_ts
      t.float      :duration
      t.string     :status     # ok, fail, running
      t.string     :err_msg
      t.timestamps
    end
    add_index :sync_log, :schema_name
    add_index :sync_log, :created_at

    create_table :sync_detail_log do |t|
      t.integer   :sync_log_id, :null => false
      t.string    :table_name,  :null => false
      t.integer   :inserted,    :default => 0
      t.integer   :updated,     :default => 0
      t.integer   :copied,      :default => 0
      t.integer   :deleted,     :default => 0
      t.float     :duration
      t.float     :src_dur
      t.float     :dst_dur
      t.timestamp :created_at
    end
    add_index :sync_detail_log, :sync_log_id

    create_table :sync_ak_table_templates do |t|
      t.string :table_name
      t.string :sync_method
      t.string :pkey
      t.string :pkey_type
    end

  end


  def down
    drop_table :sync_tables
    drop_table :sync_log
    drop_table :sync_detail_log
    drop_table :sync_ak_table_templates
  end

end
