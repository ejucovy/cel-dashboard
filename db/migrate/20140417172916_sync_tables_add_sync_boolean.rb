class SyncTablesAddSyncBoolean < ActiveRecord::Migration

  def up
    add_column :sync_tables, :sync, :boolean, :default => true
    execute("UPDATE sync_tables SET sync = false WHERE sync_method = 'ignore'")
  end

  def down
    remove_column :sync_tables, :sync
  end

end
