#
# Add indexes to AK tables.
#
require 'rubygems'
require 'bundler'

APP_PATH = File.expand_path('../../config/application',  __FILE__)
require File.expand_path('../../config/boot',  __FILE__)
require File.expand_path('../../config/environment',  __FILE__)

require 'pp'
require 'yaml'

# Extract the database names from config/database.yml
def ak_databases
  db_yaml = YAML::load(open("#{Rails.root}/config/ak_database.yml"))
  dbs = {}
  db_yaml.each do |k, v|
    dbs[k.to_sym] = v if v.class == Hash
  end
  dbs
end


def add_indexes(client)

  Apartment::Database.process(client.cc_schema) do

    Apartment.connection.execute("DROP INDEX IF EXISTS core_user_created_at_idx")
    Apartment.connection.execute("CREATE INDEX core_user_created_at_idx ON core_user (created_at)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_open_created_at_idx, core_open_mailing_id_user_id_idx")
    Apartment.connection.execute("CREATE INDEX core_open_created_at_idx ON core_open (created_at)")
    Apartment.connection.execute("CREATE INDEX core_open_mailing_id_user_id_idx ON core_open (mailing_id, user_id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_click_created_at_idx, core_click_mailing_id_user_id_idx")
    Apartment.connection.execute("CREATE INDEX core_click_created_at_idx ON core_click (created_at)")
    Apartment.connection.execute("CREATE INDEX core_click_mailing_id_user_id_idx ON core_click (mailing_id, user_id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_action_created_at_idx, core_action_multi1_idx, core_action_page_id_status")
    Apartment.connection.execute("CREATE INDEX core_action_created_at_idx ON core_action (created_at)")
    Apartment.connection.execute("CREATE INDEX core_action_multi1_idx ON core_action (mailing_id, user_id, created_at, updated_at)")
    Apartment.connection.execute("CREATE INDEX core_action_page_id_status ON core_action(page_id, status)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_mailing_started_at_id_idx")
    Apartment.connection.execute("CREATE INDEX core_mailing_started_at_id_idx ON core_mailing (started_at, id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_usermailing_created_at_idx")
    Apartment.connection.execute("CREATE INDEX core_usermailing_created_at_idx ON core_usermailing (created_at)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_mailingsubject_mailing_id_id_idx")
    Apartment.connection.execute("CREATE INDEX core_mailingsubject_mailing_id_id_idx ON core_mailingsubject (mailing_id, id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_subscriptionhistory_user_id_idx, core_subscriptionhistory_action_id_change_id_idx")
    Apartment.connection.execute("CREATE INDEX core_subscriptionhistory_user_id_idx ON core_subscriptionhistory (user_id)")
    Apartment.connection.execute("CREATE INDEX core_subscriptionhistory_action_id_change_id_idx ON core_subscriptionhistory (action_id, change_id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_order_created_at_idx, core_order_user_id_idx, core_order_action_id_idx")
    Apartment.connection.execute("CREATE INDEX core_order_created_at_idx ON core_order (created_at)")
    Apartment.connection.execute("CREATE INDEX core_order_user_id_idx ON core_order (user_id)")
    Apartment.connection.execute("CREATE INDEX core_order_action_id_idx ON core_order (action_id)")

    Apartment.connection.execute("DROP INDEX IF EXISTS core_transaction_order_id_idx")
    Apartment.connection.execute("CREATE INDEX core_transaction_order_id_idx ON core_transaction (order_id)")

  end
end


def usage(msg = nil)
  $stderr.puts "\n#{msg}\n\n" if msg
  $stderr.puts "ak-create_index.rb dbname"
  $stderr.puts ""
  $stderr.puts "    dbname must exist in config/ak_database.yml"
  $stderr.puts ""
  exit(1)
end

usage("ERROR: Wrong number of arguments") if ARGV.size != 1
dbname = ARGV[0].to_sym

if ak_databases[dbname].blank?
  usage("ERROR: unknown dbname (#{dbname})\n\n  Known databases: " + ak_databases().keys.collect{|x| x.to_s}.join(', '))
end

client = Client.find_by_ak_schema_name(dbname)

usage("ERROR: #{dbname} is not configured in the clients table") unless client

add_indexes(client)

