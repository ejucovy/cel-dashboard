SELECT unique_opens.mailing_id, 
       all_opens.cnt AS all_opens,
       unique_opens.cnt AS unique_opens,
       dup_opens.cnt AS dup_opens,
       ROUND(dup_opens.cnt / all_opens.cnt * 100, 2) dup_rate,
       ROUND(all_opens.cnt / unique_opens.cnt, 2) AS forward_factor,
       ROUND((all_opens.cnt - dup_opens.cnt) / unique_opens.cnt, 2) AS forward_factor_without_dups
  FROM
      (
        SELECT mailing_id, COUNT(1) cnt
          FROM
              (
                SELECT DISTINCT mailing_id, user_id
                  FROM core_open
                ) unique_opens_detail
         GROUP BY mailing_id
      ) as unique_opens,
      (
        SELECT mailing_id, COUNT(1) cnt
          FROM core_open
         GROUP BY mailing_id
      ) AS all_opens 
      LEFT OUTER JOIN
      (
        SELECT mailing_id, SUM(cnt) cnt
          FROM
              (
                SELECT COUNT(1) cnt, mailing_id, user_id, created_at
                  FROM core_open
                 GROUP BY mailing_id, user_id, created_at
                HAVING COUNT(1) > 1
            ) AS dup_opens_detail
      GROUP BY mailing_id
      ) dup_opens ON all_opens.mailing_id = dup_opens.mailing_id
 WHERE unique_opens.mailing_id = all_opens.mailing_id
   AND all_opens.cnt > 999
 ORDER BY dup_opens DESC
;
