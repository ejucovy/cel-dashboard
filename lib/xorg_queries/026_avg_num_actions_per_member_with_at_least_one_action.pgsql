SELECT AVG(actions) AS avg_actions
  FROM 
      (
      SELECT ca.user_id, COUNT(1) actions
        FROM core_action ca, core_page cp
       WHERE ca.page_id = cp.id
         AND ca.status = 'complete'
         AND cp.type NOT IN ('Import', 'Unsubscribe')
       GROUP BY ca.user_id
      ) AS X
;
