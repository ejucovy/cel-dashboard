SELECT cm.id mailing_id, 
       cm.expected_send_count AS sent,
       clicks.cnt AS clicks,
       ROUND(clicks.cnt / cm.expected_send_count * 100, 2) AS rate
  FROM core_mailing cm, 
       (
         SELECT count(1) AS cnt, cc.mailing_id
           FROM core_click cc
          WHERE cc.mailing_id IS NOT NULL
          GROUP by cc.mailing_id
       ) AS clicks
 WHERE cm.id = clicks.mailing_id
   AND cm.status = 'completed'
;