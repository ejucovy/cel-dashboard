 SELECT yr, wk, 
        SUM(sent)   AS sent, 
        SUM(opened) AS opened,
--        ROUND(opened/sent * 100, 2) AS overall_open_rate,
        ROUND(AVG(open_rate), 2)    AS avg_mailing_open_rate
  FROM
       ( 
         SELECT cm.id mailing_id, DATE_PART('year', cm.finished_at) AS yr, DATE_PART('week', cm.finished_at) AS wk,
                cm.expected_send_count AS sent,
                opened.cnt AS opened,
                ROUND(opened.cnt / cm.expected_send_count * 100, 2) AS open_rate
           FROM core_mailing cm,
                (
                  SELECT count(1) AS cnt, co.mailing_id
                    FROM core_open co
                   GROUP by co.mailing_id
                ) AS opened
         WHERE cm.id = opened.mailing_id
           AND cm.status = 'completed'
           AND cm.finished_at >= '2013-01-01'
       ) AS detail
 GROUP by yr, wk
 ORDER BY yr, wk
;