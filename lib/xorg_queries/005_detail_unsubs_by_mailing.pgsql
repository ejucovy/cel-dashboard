SELECT um.mailing_id,  COUNT(1) as unsubs
  FROM core_mailing as m, core_action a, core_usermailing um, core_unsubscribeaction us, core_subscriptionhistory sh
 WHERE m.id = um.mailing_id
   AND um.mailing_id = a.mailing_id
   AND um.user_id = a.user_id
   AND a.id = us.action_ptr_id
   AND a.id = sh.action_id
 GROUP BY um.mailing_id
;
