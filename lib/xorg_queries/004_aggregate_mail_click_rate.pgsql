SELECT SUM(sent)   AS "Total sent",
       AVG(sent)   AS "Avg send",
       SUM(clicks) AS "Total clicks",
       AVG(clicks) AS "Avg clicks",
       (SUM(clicks) / SUM(sent)) AS "Click rate",
       AVG(rate)   AS "Avg click rate"
  FROM
       (
        SELECT cm.id mailing_id, 
               cm.expected_send_count AS sent,
               xclicks.cnt AS clicks,
               ROUND(xclicks.cnt / cm.expected_send_count * 100, 2) AS rate
          FROM core_mailing cm,
               (
                 SELECT count(1) AS cnt, cc.mailing_id
                   FROM core_click cc
                  WHERE cc.mailing_id IS NOT NULL
                  GROUP by cc.mailing_id
               ) AS xclicks
         WHERE cm.id = xclicks.mailing_id
           AND cm.status = 'completed'
       ) AS clicks
 ;
