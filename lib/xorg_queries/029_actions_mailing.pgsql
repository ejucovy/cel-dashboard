SELECT X.mailing_id,
       X.sent,
       MAX(CASE WHEN X.page_type = 'Petition'    THEN X.cnt ELSE 0 END) AS signatures,
       MAX(CASE WHEN X.page_type = 'Donation'    THEN X.cnt ELSE 0 END) AS donations,
       MAX(CASE WHEN X.page_type = 'Call'        THEN X.cnt ELSE 0 END) AS calls,
       MAX(CASE WHEN X.page_type = 'Survey'      THEN X.cnt ELSE 0 END) AS surveys,
       MAX(CASE WHEN X.page_type = 'EventCreate' THEN X.cnt ELSE 0 END) AS event_creates,
       MAX(CASE WHEN X.page_type = 'EventSignup' THEN X.cnt ELSE 0 END) AS event_signups,
       MAX(CASE WHEN X.page_type = 'Letter'      THEN X.cnt ELSE 0 END) AS letters,
       MAX(CASE WHEN X.page_type = 'LTE'         THEN X.cnt ELSE 0 END) AS ltes,
       MAX(CASE WHEN X.page_type = 'Signup'      THEN X.cnt ELSE 0 END) AS signups
  FROM
(
SELECT actions.mailing_id,
       cm.expected_send_count sent,
       actions.page_type,
       actions.cnt
  FROM core_mailing AS cm,
       (
        SELECT COUNT(1) cnt, ca.mailing_id, cp.type AS page_type
          FROM core_action ca, core_page cp
         WHERE ca.page_id = cp.id
           AND ca.status = 'complete'
           AND cp.type NOT IN ('Import', 'Unsubscribe')
         GROUP by ca.mailing_id, cp.type
       ) AS actions
 WHERE cm.status = 'completed'
   AND cm.id = actions.mailing_id
) AS X
 GROUP BY x.mailing_id, X.sent
;
