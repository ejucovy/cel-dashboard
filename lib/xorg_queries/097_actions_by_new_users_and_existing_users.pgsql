SELECT allx.month,
       allx.cnt AS all_actions,
       new_users.cnt AS actions_that_created_members,
       no_users.cnt AS actions_from_existing_members,
       ROUND(100.0 * new_users.cnt / allx.cnt, 2) AS ratio_new_users_by_all_actions,
       ROUND(100.0 * new_users.cnt / no_users.cnt, 2) AS ratio_new_users_by_actions_from_existing_users
  FROM
(
      SELECT date_trunc('month', ca.created_at) AS month, COUNT(1) AS cnt 
        FROM core_action AS ca, core_page AS cp
       WHERE ca.page_id = cp.id
         AND ca.status = 'complete'
         AND cp.type NOT IN ('Import', 'Unsubscribe')
         AND (ca.created_at >= '2013-01-01' AND ca.created_at < '2013-08-01')
       GROUP BY month
       ORDER BY month
) AS allx,
(
      SELECT date_trunc('month', ca.created_at) AS month, COUNT(1) AS cnt 
        FROM core_action AS ca, core_page AS cp
       WHERE ca.page_id = cp.id
         AND ca.status = 'complete'
         and created_user = true
         AND cp.type NOT IN ('Import', 'Unsubscribe')
         AND (ca.created_at >= '2013-01-01' AND ca.created_at < '2013-08-01')
       GROUP BY month
       ORDER BY month
) AS new_users,
(
      SELECT date_trunc('month', ca.created_at) AS month, COUNT(1) AS cnt 
        FROM core_action AS ca, core_page AS cp
       WHERE ca.page_id = cp.id
         AND ca.status = 'complete'
         AND created_user = false
         AND cp.type NOT IN ('Import', 'Unsubscribe')
         and (ca.created_at >= '2013-01-01' AND ca.created_at < '2013-08-01')
       GROUP BY month
       ORDER BY month
) AS no_users
 WHERE allx.month = new_users.month
   AND allx.month = no_users.month
 ORDER BY allx.month
;
