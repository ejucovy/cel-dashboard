SELECT cm.id AS mailing_id, 
       cm.expected_send_count AS sent,
       opens.cnt AS opens,
       ROUND(opens.cnt / cm.expected_send_count * 100, 2) AS open_rate,
       clicks.cnt AS clicks,
       ROUND(clicks.cnt / cm.expected_send_count * 100, 2) AS click_rate,
       unsubs.unsubs,
       ROUND(unsubs.unsubs / cm.expected_send_count * 100, 2) AS unsub_rate
  FROM core_mailing cm,
       (
         SELECT count(1) AS cnt, co.mailing_id
           FROM core_open co
          GROUP by co.mailing_id
       ) AS opens,
       (
         SELECT count(1) AS cnt, cc.mailing_id
           FROM core_click cc
          WHERE cc.mailing_id IS NOT NULL
          GROUP by cc.mailing_id
       ) AS clicks,
       (
        SELECT um.mailing_id,  
              COUNT(1) as unsubs
          FROM core_mailing as m, core_action a, core_usermailing um, core_unsubscribeaction us, core_subscriptionhistory sh
         WHERE m.id = um.mailing_id
           AND um.mailing_id = a.mailing_id
           AND um.user_id = a.user_id
           AND a.id = us.action_ptr_id
           AND a.id = sh.action_id
         GROUP BY um.mailing_id
       ) AS unsubs
 WHERE cm.status = 'completed'
   AND cm.id = opens.mailing_id
   AND cm.id = clicks.mailing_id
   AND cm.id = unsubs.mailing_id
;