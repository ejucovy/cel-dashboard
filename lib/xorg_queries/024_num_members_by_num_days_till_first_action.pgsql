SELECT cu.created_at - ca.created_at AS days_till_first_action,
       COUNT(1) AS members
  FROM core_user cu, core_action ca,
       (
         SELECT MIN(cax.id) AS id
           FROM core_action cax
          WHERE cax.created_user = false
            AND cax.status = 'complete'
          GROUP BY cax.user_id
       ) AS first_action
 WHERE cu.id = ca.user_id
   and ca.id = first_action.id
 GROUP BY days_till_first_action
 ORDER BY days_till_first_action
;
