SELECT SUM(sent)   AS "Total sent",
       AVG(sent)   AS "Avg send",
       SUM(opens)  AS "Total open",
       AVG(opens)  AS "Avg open",
       ROUND(SUM(opens) / SUM(sent) * 100, 2) AS "Open rate",
       AVG(open_rate) AS "Avg open rate",
       SUM(clicks) AS "Total click",
       AVG(clicks) AS "Avg click",
       ROUND( SUM(clicks) / SUM(sent) * 100, 2) AS "Click rate",
       AVG(click_rate)   AS "Avg click rate",
       SUM(unsubs) AS "Total unsubs",
       AVG(unsubs) AS "Avg unsubs",
       ROUND(SUM(unsubs) / SUM(sent) * 100, 2) AS "Unsub rate",
       AVG(unsub_rate) AS "Avg unsub rate"
  FROM
       (
        SELECT cm.id AS mailing_id, 
               cm.expected_send_count AS sent,
               opens,
               ROUND(opens / cm.expected_send_count * 100, 2) AS open_rate,
               clicks,
               ROUND(clicks / cm.expected_send_count * 100, 2) AS click_rate,
               unsubs,
               ROUND(unsubs / cm.expected_send_count * 100, 2) AS unsub_rate
          FROM core_mailing cm,
               (
                 SELECT count(1) AS opens, co.mailing_id
                   FROM core_open co
                  GROUP by co.mailing_id
               ) AS opens,
               (
                 SELECT count(1) AS clicks, cc.mailing_id
                   FROM core_click cc
                  WHERE cc.mailing_id IS NOT NULL
                  GROUP by cc.mailing_id
               ) AS clicks,
               (
                SELECT COUNT(1) as unsubs, um.mailing_id
                  FROM core_mailing as m, core_action a, core_usermailing um, core_unsubscribeaction us, core_subscriptionhistory sh
                 WHERE m.id = um.mailing_id
                   AND um.mailing_id = a.mailing_id
                   AND um.user_id = a.user_id
                   AND a.id = us.action_ptr_id
                   AND a.id = sh.action_id
                 GROUP BY um.mailing_id
               ) AS unsubs
         WHERE cm.status = 'completed'
           AND cm.id = opens.mailing_id
           AND cm.id = clicks.mailing_id
           AND cm.id = unsubs.mailing_id
       ) as detail
;
