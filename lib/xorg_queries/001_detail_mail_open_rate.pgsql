SELECT cm.id mailing_id, 
       cm.expected_send_count AS sent,
       opened.cnt AS opened,
       ROUND(opened.cnt / cm.expected_send_count * 100, 2) AS "Open rate %"
  FROM core_mailing cm,
       (
         SELECT count(1) AS cnt, co.mailing_id
           FROM core_open co
          GROUP by co.mailing_id
       ) AS opened
 WHERE cm.id = opened.mailing_id
   AND cm.status = 'completed'
;