SELECT total_members.cnt AS total_members,
       mailable_members.cnt AS mailable_members,
       donors.cnt AS donors,
       ROUND(donors.cnt / total_members.cnt * 100, 2) AS pct_total_members_that_donate,
       ROUND(donors.cnt / mailable_members.cnt * 100, 2) AS  pct_mailable_members_that_donate
  FROM
       (
        SELECT COUNT(1) AS cnt from core_user
       ) AS total_members,
       (
        SELECT COUNT(DISTINCT(cs.user_id)) AS cnt
          FROM core_list cl, core_subscription cs
         WHERE cl.is_default = true
           AND cl.id = cs.list_id
       ) AS mailable_members,
       (
        SELECT COUNT(DISTINCT user_id) AS cnt from core_order
       ) as donors
;