SELECT DATE(co.created_at) - DATE(cu.created_at) AS days_till_first_donation,
       COUNT(1) AS members
  FROM core_user cu, core_order co,
       (
         SELECT MIN(cox.id) AS id
           FROM core_order cox, core_transaction ctx
          WHERE cox.total > 0
            AND cox.id = ctx.order_id
            AND ctx.status = 'completed'
          GROUP BY cox.user_id
       ) AS first_order
 WHERE cu.id = co.user_id
   AND co.id = first_order.id
   AND co.created_at - cu.created_at > interval '5 seconds'
 GROUP BY days_till_first_donation
 ORDER BY days_till_first_donation
;
