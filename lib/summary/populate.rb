require 'summary/populate/cleanup_subscriptionhistory'
require 'summary/populate/domains'
require 'summary/populate/email_action_summary'
require 'summary/populate/email_action_summary_by_period'
require 'summary/populate/email_sent_summary'
require 'summary/populate/sources'
require 'summary/populate/list_size'
require 'summary/populate/list_size_by_period'
require 'summary/populate/page_action_summary'
require 'summary/populate/page_action_summary_by_period'


module Summary
  module Populate
    class << self

  def refresh(client)
    t1 = Time.now
    t = Time.now
    cleanup_subscriptionhistory(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): cleanup_subscriptionhistory(): #{Time.now - t} seconds"

    t = Time.now
    domains_from_user_emails(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): domains_from_user_emails(): #{Time.now - t} seconds"

    t = Time.now
    domain_users(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): domain_users(): #{Time.now - t} seconds"

    t = Time.now
    domain_email_counts(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): domain_email_counts(): #{Time.now - t} seconds"

    t = Time.now
    sources_from_users(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): sources_from_users(): #{Time.now - t} seconds"

    t = Time.now
    source_users(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): source_users(): #{Time.now - t} seconds"

    t = Time.now
    source_user_counts(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): source_user_counts(): #{Time.now - t} seconds"

    t = Time.now
    sources_from_actions(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): sources_from_actions(): #{Time.now - t} seconds"

    t = Time.now
    source_actions(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): source_actions(): #{Time.now - t} seconds"

    t = Time.now
    source_action_counts(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): source_action_counts(): #{Time.now - t} seconds"

    t = Time.now
    email_action_summary(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): email_action_summary(): #{Time.now - t} seconds"

    t = Time.now
    email_action_summary_by_period(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): email_action_summary_by_period(): #{Time.now - t} seconds"

    t = Time.now
    email_sent_summary(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): email_sent_summary(): #{Time.now - t} seconds"

    t = Time.now
    page_action_summary(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): page_action_summary(): #{Time.now - t} seconds"

    t = Time.now
    page_action_summary_by_period(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): page_action_summary_by_period(): #{Time.now - t} seconds"

    t = Time.now
    list_size(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): list_size(): #{Time.now - t} seconds"

    t = Time.now
    list_size_by_period(client)
    puts "    Summary::Populate.refresh(#{client.ak_schema}): list_size_by_period(): #{Time.now - t} seconds"

    puts "  Summary::Populate.refresh(#{client.ak_schema}): total: #{Time.now - t1} seconds"
  end

    end
  end
end
