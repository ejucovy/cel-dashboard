module Summary
  module Query
    class << self

  #
  # params:
  #   - page_ids => "1,2,3" or [mailing_id1, mailing_id2, ...]
  #   - activitystart_date => dt
  #   - activityend_date => dt
  #   - mailing_ids => "1,2,3" or [tag1, tag2, ...]
  #   - tag_ids => "1,2,3" or [tag1, tag2, ...]
  #   - limit => n
  #
  # Delayed until campaigns exist
  #   - campaigns => [campaign1, campaign2, ...]
  #
  def page_summary(client, params = {}, json: false, tz: 'UTC', limit: nil)
    return [] if client.nil? || client.cc_schema.blank?

    params.symbolize_keys! if params && params.class == Hash
    params = params.dup
    if params[:page_ids].blank? && params[:activity_start_dt].blank? && params[:activity_end_dt].blank?
      params[:activity_start_dt] = 2.weeks.ago
    end

    pages_where = ""
    pages_where = "AND pas.page_id IN (#{param_for_sql_in(params[:page_ids])})" if params[:page_ids].present?

    activity_dt_where = ""
    activity_dt_where << "AND period >= '#{params[:activity_start_dt]}' " if params[:activity_start_dt].present?
    activity_dt_where << "AND period < '#{params[:activity_end_dt]}' " if params[:activity_end_dt].present?

    mailings_where = ""
    mailings_where = "AND pas.mailing_id IN (#{param_for_sql_in(params[:mailing_ids])})" if params[:mailing_ids].present?

    tags_table = ""
    tags_where = ""
    if params[:tag_ids].present?
      tags_table = "core_page_tags cpt,"
      tags_where = "AND cpt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cpt.page_id = pas.page_id "
    end

    sql = %Q{ 
               WITH
               ps AS (
                 SELECT pas.page_id,
                        SUM(pas.actions) AS actions,
                        SUM(pas.new_members) AS new_members,
                        SUM(pas.new_subscribers) AS new_subscribers,
                        SUM(pas.revenue) AS revenue
                   FROM #{tags_table}
                        page_action_summary_by_period pas
                  WHERE 1 = 1
                    #{pages_where}
                    #{activity_dt_where}
                    #{mailings_where}
                    #{tags_where}
                  GROUP BY pas.page_id
               ),
                aps AS (
                  SELECT AVG(actions) AS action_avg from ps
                )    
               SELECT p.title, 
                     p.type,
                     p.created_at,
                     l.list_size,
                     ps.page_id,
                     ps.actions,
                     aps.action_avg,
                     CASE WHEN p.type = 'Unsubscribe' THEN  -ps.actions ELSE ps.new_members END AS members,
                     ps.new_subscribers,
                     ps.revenue
                FROM core_page p,
                     ps,
                     aps,
                     (
                      SELECT ROUND(AVG(mailable)) AS list_size
                        FROM list_size_by_period ls,
                             core_list cl
                       WHERE cl.is_default = true
                         AND cl.id = ls.list_id
                         #{activity_dt_where}
                     ) AS l
               WHERE p.id = ps.page_id
               ORDER BY ps.actions DESC
    }

    sql << "LIMIT #{limit}" if limit.to_i > 0

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)

  end

    end
  end
end
