module Summary
  module Query
    class << self

  def list_size_time_series(client, list_id: nil, period: 'day', json: false, tz: 'UTC')

    case period
      when 'minute', 'min'
        seconds = 60
      when 'hour'
        seconds = 60 * 60
      when 'day'
        seconds = 60 * 60 * 24
      when 'week'
        seconds = 60 * 60 * 24 * 7
      when 'month'
        seconds = 60 * 60 * 24 * 30
      when 'quarter'
        seconds = 60 * 60 * 24 * 30 * 3
      else 
        seconds = 60 * 60 * 24  # day
    end

    if list_id
      list_where = "AND cl.id = #{list_id} "
    else
      list_where = "AND cl.is_default = true "
    end

    sql = %Q{
              SELECT ls.period AT TIME ZONE '#{tz}' AS period,
                     ls.list_id,
                     ls.mailable
                FROM list_size_by_period ls,
                     core_list cl
               WHERE ls.list_id = cl.id
                 #{list_where}
                 AND (EXTRACT(epoch FROM ls.period AT TIME ZONE '#{tz}')::INTEGER % #{seconds}) = 0
               ORDER BY ls.period
    }

    rows = []
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)
  end

    end
  end
end
