module Summary
  module Query
    class << self

  def mailing_comparison(client, params)
    return [] if client.nil? || client.cc_schema.blank?
    return [] if params[:mailing_ids].blank?  

    if params[:duration].present?
      duration = params[:duration]
    else
      duration = '24 hours'
    end

    mailing_ids = param_for_sql_in(params[:mailing_ids])

    sql = %Q{
              SELECT easp.mailing_id, mmeta.sent, easp.period - mmeta.period dur_since_sent, 
                     SUM(easp.opens) opens, 
                     SUM(easp.clicks) clicks, 
                     SUM(easp.actions) actions,
                     SUM(unsubs) unsubs,
                     SUM(unsub_complaints) unsub_complaints,
                     SUM(unsub_bounces) unsub_bounces,
                     SUM(donations) donations,
                     SUM(revenue) revenue
                FROM 
                     (
                       SELECT cm.id, cm.progress sent, MIN(eas.period) period 
                         FROM core_mailing cm, email_action_summary_by_period eas
                        WHERE cm.id IN (#{mailing_ids})
                          AND cm.id = eas.mailing_id
                        GROUP BY cm.id
                     ) AS mmeta,
                     email_action_summary_by_period easp
               WHERE mmeta.id = easp.mailing_id
                 AND easp.mailing_id IN (#{mailing_ids})
                 AND easp.period - mmeta.period < INTERVAL '#{duration}'
               GROUP BY easp.mailing_id, mmeta.sent, easp.period - mmeta.period 
               ORDER BY easp.mailing_id, mmeta.sent, easp.period - mmeta.period 
    }

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

  end

    end
  end
end


