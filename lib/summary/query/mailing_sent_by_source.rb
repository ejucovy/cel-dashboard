module Summary
  module Query
    class << self

  def mailing_sent_by_source(client, mailing_id, limit = 50, json = false)
    sql = %Q{
             SELECT s.source,
                    SUM(ess.sent) AS sent,
                    SUM(eas.opens) AS opens,
                    SUM(eas.opens_unique) AS opens_unique,
                    SUM(eas.clicks) AS clicks,
                    SUM(eas.clicks_unique) AS clicks_unique,
                    SUM(eas.actions) AS actions,
                    SUM(new_members) AS new_members,
                    SUM(unsubs) AS unsubs,
                    SUM(unsub_complaints) AS unsub_complaints,
                    SUM(unsub_bounces) AS unsub_bounces,
                    SUM(donations) AS donations,
                    SUM(revenue) AS revenue
               FROM email_sent_summary ess,
                    email_action_summary eas,
                    top_user_sources s
              WHERE ess.mailing_id = eas.mailing_id
                AND ess.subject_id = eas.subject_id
                AND ess.email_domain_id = eas.email_domain_id
                AND ess.user_source_id = eas.user_source_id
                AND eas.user_source_id = s.id
                AND eas.mailing_id = ?
              GROUP BY s.source
              ORDER BY SUM(ess.sent) DESC
              LIMIT ?
            }

    sql = ActiveRecord::Base.escape_sql([sql, mailing_id, limit])

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)
  end

    end
  end
end
