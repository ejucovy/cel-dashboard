module Summary
  module Query
    class << self

  def list_stats(client, list_id: nil, time_frame: 'TW', json: false)

    case time_frame
      when 'TW'       # trailing week (7 days)
        period = "CURRENT_DATE - INTERVAL '7 days'"
      when 'TM'       # trailing month (30 days)
        period = "CURRENT_DATE - INTERVAL '30 days'"
      when 'TQ'       # trailing quarter (90 days)
        period = "CURRENT_DATE - INTERVAL '90 days'"
      when 'TY'       # trailing year (365 days)
        period = "CURRENT_DATE - INTERVAL '365 days'"

      when 'WTD'      # week to date (since Sunday of current week)
        period = "CURRENT_DATE - EXTRACT(DOW FROM CURRENT_DATE)::INTEGER"
      when 'MTD'      # month to date (since the 1st of the current month)
        period = "CURRENT_DATE - EXTRACT(DAY FROM CURRENT_DATE)::INTEGER + 1"
      when 'QTD'      # quarter to date (since the 1st of the current quarter: Jan, April, July, Oct)
        period = "DATE_TRUNC('quarter', CURRENT_DATE)::DATE"
      when 'YTD'      # year to date (since Jan 1 of current year)
        period = "CURRENT_DATE - EXTRACT(DOY FROM CURRENT_DATE)::INTEGER"
      when 'ALL'
        period = "'1070-01-01'"

      else            # default to TW
        period = "CURRENT_DATE - INTERVAL '7 days'"
    end

    if list_id.nil?
      Apartment::Database.process(client.cc_schema) do
        list_id = AkList.default.id
      end
    end

    sql = %Q{
              WITH a AS
              (
              SELECT SUM(pas.actions) AS actions,
                     SUM(pas.revenue) AS revenue
                FROM page_action_summary_by_period pas,
                     core_page cp
               WHERE pas.page_id = cp.id
                 AND cp.list_id = #{list_id}
                 AND pas.period >= #{period}
              ),
              l AS
              (
              SELECT SUM(ls.subs) AS subs,
                     SUM(ls.unsubs) AS unsubs
                FROM list_size_by_period ls
               WHERE ls.list_id = #{list_id}
                 AND ls.period >= #{period}
              ),
              m AS
              (
              SELECT mailable
                FROM list_size ls
               WHERE ls.list_id = #{list_id}
               ORDER BY ls.id DESC
               LIMIT 1
              )
              SELECT actions, revenue, subs, unsubs, mailable
                FROM a, l, m
    }

    row = nil
    Apartment::Database.process(client.cc_schema) do
      row = Apartment.connection.execute(sql).first
    end

    return row if json == false

    Yajl::Encoder.encode(row)
  end

    end
  end
end
