module Summary
  module Query
    class << self

  #
  # params:
  #   - mailing_ids => "1,2,3" or [mailing_id1, mailing_id2, ...]
  #   - mailing_start_date => dt
  #   - mailing_end_date => dt
  #   - activity_start_date => dt
  #   - activity_end_date => dt
  #   - domain_ids => "1,2,3" or [d1, d2, d3, ...]
  #   - tag_ids => "1,2,3" or [tag1, tag2, ...]
  #   - limit => n
  #
  # Delayed until campaigns exist
  #   - campaigns => [campaign1, campaign2, ...]
  #
  def mailing_summary(client, params = {}, json: false, tz: 'UTC', limit: nil, include_subjects: true)

    return [] if client.nil? || client.cc_schema.blank?

    params.symbolize_keys! if params && params.class == Hash
    params = params.dup

    params[:mailing_start_dt] = 2.weeks.ago if params[:mailing_start_dt].nil?
    params[:mailing_ids] = params[:mailing_id] if params[:mailing_id].present? && params[:mailing_ids].blank?

    mailings_where = ""
    mailings_where = "AND eas.mailing_id in (#{param_for_sql_in(params[:mailing_ids])})" if params[:mailing_ids].present?

    mailing_dt_where = ""
    mailing_dt_where << "AND cm.started_at >= '#{params[:mailing_start_dt]}' " if params[:mailing_start_dt].present?
    mailing_dt_where << "AND cm.started_at < '#{params[:mailing_end_dt]}' " if params[:mailing_end_dt].present?

    eas_domain_where = ""
    ess_domain_where = ""
    if params[:domain_ids].present?
      eas_domain_where = "AND eas.email_domain_id IN (#{param_for_sql_in(params[:domain_ids])}) " 
      ess_domain_where = "AND ess.email_domain_id IN (#{param_for_sql_in(params[:domain_ids])}) " 
    end

    eas_user_source_where = ""
    ess_user_source_where = ""
    if params[:user_source_ids].present?
      eas_user_source_where = "AND eas.user_source_id IN (#{param_for_sql_in(params[:user_source_ids])}) " 
      ess_user_source_where = "AND ess.user_source_id IN (#{param_for_sql_in(params[:user_source_ids])}) " 
    end

    tags_table = ""
    tags_where = ""
    if params[:tag_ids].present?
      tags_table = "core_mailing_tags cmt,"
      tags_where = "AND cmt.tag_id IN (#{param_for_sql_in(params[:tag_ids])}) AND cmt.mailing_id = eas.mailing_id "
    end

    sql = %Q{ 
      SELECT eas.mailing_id,
             cm.submitter_id, au.username, au.first_name, au.last_name, au.email,
             CAST(cm.started_at  AS TIMESTAMP WITH TIME ZONE) AT TIME ZONE '#{tz}' AS started_at, 
             CAST(cm.finished_at AS TIMESTAMP WITH TIME ZONE) AT TIME ZONE '#{tz}' AS finished_at,
             cm.status, cm.notes,
             eas.subject_id, eas.subject, 
             ess.sent,
             eas.opens, eas.opens_unique, eas.clicks, eas.actions, eas.new_members, 
             eas.unsubs, 
             eas.unsub_complaints, 
             eas.unsub_bounces,
             eas.donations,
             eas.revenue
        FROM core_mailing cm,
             auth_user au,
             (
              SELECT mailing_id, subject_id, sent
                FROM
                    (
                      SELECT ess.mailing_id, 0 AS subject_id, SUM(sent) AS sent
                        FROM email_sent_summary ess
                       WHERE 1 = 1
                             #{ess_domain_where}
                             #{ess_user_source_where}
                       GROUP BY ess.mailing_id
                     UNION
                      SELECT ess.mailing_id, ess.subject_id, SUM(sent) AS sent
                        FROM email_sent_summary ess
                       WHERE 1 = 1
                             #{ess_domain_where}
                             #{ess_user_source_where}
                       GROUP BY ess.mailing_id, ess.subject_id
                  ) AS agg
             ) AS ess,
             (
              SELECT eas.mailing_id, 0 AS subject_id, '' AS subject,
                     SUM(eas.opens) as opens,
                     SUM(eas.opens_unique) as opens_unique,
                     SUM(eas.clicks) AS clicks,
                     SUM(eas.actions) AS actions,
                     SUM(new_members) AS new_members,
                     SUM(unsubs) AS unsubs,
                     SUM(unsub_complaints) AS unsub_complaints,
                     SUM(unsub_bounces) AS unsub_bounces,
                     SUM(donations) AS donations,
                     SUM(revenue) AS revenue
                FROM #{tags_table}
                     email_action_summary AS eas,
                     core_mailing AS cm
               WHERE eas.mailing_id = cm.id
                 #{mailings_where}
                 #{mailing_dt_where}
                 #{eas_domain_where}
                 #{eas_user_source_where}
                 #{tags_where}
               GROUP BY eas.mailing_id
    }
    if include_subjects
      sql << %Q{
            UNION
              SELECT eas.mailing_id, eas.subject_id, cms.text AS subject,
                     SUM(eas.opens) AS opens,
                     SUM(eas.opens_unique) as opens_unique,
                     SUM(eas.clicks) AS clicks,
                     SUM(eas.actions) AS actions,
                     SUM(new_members) AS new_members,
                     SUM(eas.unsubs) AS unsubs,
                     SUM(unsub_complaints) AS unsub_complaints,
                     SUM(unsub_bounces) AS unsub_bounces,
                     SUM(donations) AS donations,
                     SUM(revenue) AS revenue
                FROM #{tags_table}
                     email_action_summary AS eas,
                     core_mailing AS cm,
                     core_mailingsubject AS cms
               WHERE eas.mailing_id = cm.id
                 AND eas.mailing_id = cms.mailing_id
                 AND eas.subject_id = cms.id
                 #{mailings_where}
                 #{mailing_dt_where}
                 #{eas_domain_where}
                 #{eas_user_source_where}
                 #{tags_where}
               GROUP BY eas.mailing_id, eas.subject_id, cms.text
      }
    end

    sql << %Q{
             ) AS eas
       WHERE cm.id = ess.mailing_id
         AND ess.mailing_id = eas.mailing_id
         AND ess.subject_id = eas.subject_id
         AND cm.submitter_id = au.id
       ORDER BY eas.mailing_id DESC, eas.subject_id
    }

    sql << "LIMIT #{limit}" if limit.to_i > 0

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)

    # data = []
    # rows.each do |r|
    #   data << {
    #            mailing_id: r['mailing_id'].to_i,
    #            submitter_id: r['submitter_id'].to_i,
    #            username: r['username'],
    #            first_name: r['first_name'],
    #            last_name: r['last_name'],
    #            email: r['email'],
    #            started_at: r['started_at'],
    #            finished_at: r['finished_at'],
    #            status: r['status'],
    #            notes: r['notes'],
    #            subject_id: r['subject_id'].to_i,
    #            subject: r['subject'],
    #            sent: r['sent'].to_i,
    #            opens: r['opens'].to_i,
    #            clicks: r['clicks'].to_i,
    #            actions: r['actions'].to_i,
    #            new_members: r['new_members'].to_i,
    #            unsubs: r['unsubs'].to_i,
    #            unsub_complaints: r['unsub_complaints'].to_i,
    #            unsub_bounces: r['unsub_bounces'].to_i,
    #            donations: r['donations'].to_i,
    #            revenue: r['revenue'].to_f,

    #            opens_pct: r['opens'].to_f / r['sent'].to_f,
    #            clicks_pct: r['clicks'].to_f / r['sent'].to_f,
    #            actions_pct: r['actions'].to_f / r['sent'].to_f,
    #            unsubs_pct: (r['unsubs'].to_f + r['unsub_complaints'].to_f + r['unsub_bounces'].to_f) / r['sent'].to_f
    #   }
    # end

    # Yajl::Encoder.encode(data)

  end

    end
  end
end
