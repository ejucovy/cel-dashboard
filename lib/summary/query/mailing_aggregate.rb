module Summary
  module Query
    class << self

  def mailing_aggregate(client, period = 'week', json: false)
    case period
      when 'day'
        period = '1 day'
      when 'week'
        period = '1 week'
      when 'month'
        period = '1 month'
      when 'quarter'
        period = '3 months'
      when 'year'
        period = '1 year'
      else
        period = '1 week'
    end

    sql = %Q{
              SELECT CASE WHEN s.sent             IS NULL THEN 0 ELSE s.sent             END AS sent,
                     CASE WHEN a.opens            IS NULL THEN 0 ELSE a.opens            END AS opens,
                     CASE WHEN a.opens_unique     IS NULL THEN 0 ELSE a.opens_unique     END AS opens_unique,
                     CASE WHEN a.clicks           IS NULL THEN 0 ELSE a.clicks           END AS clicks,
                     CASE WHEN a.clicks_unique    IS NULL THEN 0 ELSE a.clicks_unique    END AS clicks_unique,
                     CASE WHEN a.actions          IS NULL THEN 0 ELSE a.actions          END AS actions,
                     CASE WHEN a.new_members      IS NULL THEN 0 ELSE a.new_members      END AS new_members,
                     CASE WHEN a.unsubs           IS NULL THEN 0 ELSE a.unsubs           END AS unsubs,
                     CASE WHEN a.unsub_complaints IS NULL THEN 0 ELSE a.unsub_complaints END AS unsub_complaints,
                     CASE WHEN a.unsub_bounces    IS NULL THEN 0 ELSE a.unsub_bounces    END AS unsub_bounces,
                     CASE WHEN a.donations        IS NULL THEN 0 ELSE a.donations        END AS donations,
                     CASE WHEN a.revenue          IS NULL THEN 0 ELSE a.revenue          END AS revenue
                FROM
                     (
                      SELECT SUM(opens) AS opens,
                             SUM(opens_unique) AS opens_unique,
                             SUM(clicks) AS clicks,
                             SUM(clicks_unique) AS clicks_unique,
                             SUM(actions) AS actions,
                             SUM(new_members) AS new_members,
                             SUM(unsubs) AS unsubs,
                             SUM(unsub_complaints) AS unsub_complaints,
                             SUM(unsub_bounces) AS unsub_bounces,
                             SUM(donations) AS donations,
                             SUM(revenue) AS revenue
                        FROM core_mailing cm, email_action_summary eas
                       WHERE cm.id = eas.mailing_id
                         AND cm.started_at >= now() - INTERVAL '#{period}'
                     ) AS a,
                     (
                      SELECT SUM(ess.sent) AS sent
                        FROM core_mailing cm,
                             email_sent_summary ess
                       WHERE cm.id = ess.mailing_id
                         AND cm.started_at >= now() - INTERVAL '#{period}'
                      ) AS s
      }

    rows = nil
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    Yajl::Encoder.encode(rows.to_a)

  end

    end
  end
end
