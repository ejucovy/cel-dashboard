module Summary
  module Query
    class << self

  def list_activity(client, list_ids: nil, time_frame: 'TW', tz: 'UTC', json: false)

    case time_frame
      when 'TW'       # trailing week (7 days)
        period   = "tstrunc(period AT TIME ZONE '#{tz}', '86400 SECONDS')::date"
        duration = "CURRENT_DATE - INTERVAL '7 days'"
      when 'TM'       # trailing month (30 days)
        period   = "tstrunc(period AT TIME ZONE '#{tz}', '86400 SECONDS')::date"
        duration = "CURRENT_DATE - INTERVAL '30 days'"
      when 'TQ'       # trailing quarter (90 days)
        period   = "tstrunc(period AT TIME ZONE '#{tz}', '604800 SECONDS')::date"
        duration = "CURRENT_DATE - INTERVAL '90 days'"
      when 'TY'       # trailing year (365 days)
        period   = "tstrunc(period AT TIME ZONE '#{tz}', '604800 SECONDS')::date"
        duration = "CURRENT_DATE - INTERVAL '365 days'"

      when 'WTD'      # week to date (since Sunday of current week)
        period   = "(DATE_TRUNC('DAY', period AT TIME ZONE '#{tz}'))::DATE"
        duration = "CURRENT_DATE - EXTRACT(DOW FROM CURRENT_DATE)::INTEGER"
      when 'MTD'      # month to date (since the 1st of the current month)
        period   = "(DATE_TRUNC('DAY', period AT TIME ZONE '#{tz}'))::DATE"
        duration = "CURRENT_DATE - EXTRACT(DAY FROM CURRENT_DATE)::INTEGER + 1"
      when 'QTD'      # quarter to date (since the 1st of the current quarter: Jan, April, July, Oct)
        period   = "(DATE_TRUNC('week', period AT TIME ZONE '#{tz}') + INTERVAL '1 day')::DATE"
        duration = "DATE_TRUNC('quarter', CURRENT_DATE)::DATE"
      when 'YTD'      # year to date (since Jan 1 of current year)
        period   = "(DATE_TRUNC('week', period AT TIME ZONE '#{tz}') + INTERVAL '1 day')::DATE"
        duration = "CURRENT_DATE - EXTRACT(DOY FROM CURRENT_DATE)::INTEGER"
      when 'ALL'
        period   = "(DATE_TRUNC('week', period AT TIME ZONE '#{tz}') + INTERVAL '1 day')::DATE"
        duration = "'1070-01-01'"

      else            # default to TW
        period   = "tstrunc(period  AT TIME ZONE '#{tz}', '86400 SECONDS')::date"
        duration = "CURRENT_DATE - INTERVAL '7 days'"
    end

    # 
    # First get the default list ID or validate the provided ids
    #
    if list_ids.nil?
      sql = %q{ SELECT id FROM core_list WHERE is_default = TRUE}
    else
      sql = %Q{ SELECT id FROM core_list WHERE id IN ( #{param_for_sql_in(list_ids)} ) }
    end
    rows = []
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end
    list_ids = rows.collect{|r| r['id']}.join(', ')


    #
    # The following attempts to explain why the queries below are as
    # complicated as they are. (Hint, it shouldn't be this complicated...)
    #
    # The summary table list_size_by_period is populated by replaying 
    # core_subscriptionhistory (csh) from the beginning of time. Each
    # row in csh represents a subscription change event (sub or unsub)
    # for a given user, list and timestamp. The sum of all subs minus
    # the sum of all unsubs (accounting for user_id) should equal 
    # mailable users. It comes close for some PCs, for others it doesn't.
    #
    # Since the values in list_size match AK's #s, the delta is calculated
    # between list_size and the most recent row in list_size_by_period.
    # This difference is then subtracted from mailable for each row
    # in list_size_by_period. Very hacky, but produces an acturate-ish
    # time line.
    #
    sql = %Q{
              WITH als AS
              (
                SELECT SUM(ls.mailable) AS mailable
                  FROM list_size ls
                 WHERE ls.list_id IN (#{list_ids})
              ), 
              cls AS 
              (
                SELECT SUM(lsbp.mailable) AS mailable
                  FROM list_size_by_period lsbp
                 WHERE lsbp.id IN (SELECT MAX(lsbpx.id) FROM list_size_by_period lsbpx WHERE lsbpx.list_id IN (#{list_ids}))
              )
              SELECT SUM(cls.mailable - als.mailable) AS delta
                FROM als, cls
    }

    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end
    delta = rows[0]['delta']

    sql = %Q{
              WITH l AS
              (
                SELECT #{period} AS period,
                       SUM(ls.subs) AS new_members,
                       SUM(ls.unsubs) AS unsubs,
                       ROUND(AVG(ls.mailable - #{delta})) AS mailable
                  FROM list_size_by_period ls
                 WHERE ls.list_id IN (#{list_ids})
                   AND ls.period >= #{duration}
                 GROUP BY #{period}
              ), 
              a AS
              (
                SELECT #{period} AS period,
                       SUM(p.actions) AS actions
                  FROM page_action_summary_by_period p
                 WHERE p.period >= #{duration}
                 GROUP BY #{period}
              )
              SELECT l.period,
                     EXTRACT(EPOCH FROM l.period)::bigint * 1000 AS period_epoch,
                     l.new_members, l.unsubs, a.actions, l.mailable
                FROM l LEFT OUTER JOIN a ON l.period = a.period
               ORDER BY l.period
    }

    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    return rows if json == false

    data = []
    rows.each do |r|
      data << { period:       r['period_epoch'].to_i,
                new_members:  r['new_members'].to_i,
                unsubs:       r['unsubs'].to_i,
                actions:      r['actions'].to_i,
                mailable:     r['mailable'].to_i
              }
    end

    Yajl::Encoder.encode(data)
  end

    end
  end
end
