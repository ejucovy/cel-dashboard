module Summary
  module Populate
    class << self

  private

  def populate_mailing_max_events(client)
    t = Time.now

    sql1 = %Q{ TRUNCATE #{client.cc_schema}.mailing_max_events RESTART IDENTITY }

    sql2 = %Q{ INSERT INTO #{client.cc_schema}.mailing_max_events (mailing_id, max_opened_at, max_clicked_at, max_actioned_at) 
                 SELECT * FROM #{client.cc_schema}.mailing_max_events_view
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql1)
      Apartment.connection.execute(sql2)
    end
    puts "        Summary::Populate.email_action_summary(#{client.ak_schema}) populate_mailing_max_events(): #{Time.now - t} seconds"
  end


  def determine_mailings_to_refresh(client)
    t = Time.now

    sql1 = %Q{ SELECT DISTINCT cm.id AS mailing_id
                 FROM #{client.ak_schema}.core_mailing cm
                      LEFT OUTER JOIN #{client.cc_schema}.email_action_summary eas ON cm.id = eas.mailing_id
                WHERE cm.status in ('completed', 'stopped', 'died')
                  AND cm.started_at IS NOT NULL
                  AND eas.mailing_id IS NULL
    }

    sql2 = %Q{ SELECT mme.mailing_id
                 FROM #{client.cc_schema}.mailing_max_events mme,
                      (SELECT DISTINCT mailing_id, max_opened_at, max_clicked_at, max_actioned_at FROM #{client.cc_schema}.email_action_summary) eas
                WHERE mme.mailing_id = eas.mailing_id
                  AND (mme.max_opened_at > eas.max_opened_at
                       OR 
                       mme.max_clicked_at > eas.max_clicked_at
                       OR 
                       mme.max_actioned_at > eas.max_actioned_at)
    }

    mailing_ids = []
    Apartment::Database.process(client.cc_schema) do
      mailing_ids  = Apartment.connection.execute(sql1).collect{|r| r['mailing_id']}
      mailing_ids += Apartment.connection.execute(sql2).collect{|r| r['mailing_id']}
    end

    puts "        Summary::Populate.email_action_summary(#{client.ak_schema}) determine_mailings_to_refresh(): #{Time.now - t} seconds"

    mailing_ids
  end


  def update_email_action_summary(client, mailing_ids)
    t = Time.now

    mailing_ids = mailing_ids.join(', ')

    sql1 = %Q{ DELETE FROM email_action_summary WHERE mailing_id IN (#{mailing_ids}) }

    sql2 = %Q{ 
              INSERT INTO email_action_summary
                           (
                              mailing_id, subject_id,
                              email_domain_id, user_source_id,
                              opens, opens_unique,
                              clicks, clicks_unique,
                              actions, new_members,
                              unsubs, unsub_complaints, unsub_bounces,
                              donations, revenue,
                              max_opened_at, max_clicked_at, max_actioned_at
                           )
              SELECT mailing_id, subject_id, 
                     email_domain_id, source_id,
                     SUM(opens) AS opens,
                     SUM(opens_unique) AS opens_unique,
                     SUM(clicks) AS clicks,
                     SUM(clicks_unique) AS clicks_unique,
                     SUM(actions) AS actions,
                     SUM(new_members) AS new_members,
                     SUM(unsubs) AS unsubs,
                     SUM(unsub_complaints) AS unsub_complaints,
                     SUM(unsub_bounces) AS unsub_bounces,
                     SUM(donations) AS donations,
                     SUM(revenue) AS revenue,
                     max_opened_at,
                     max_clicked_at,
                     max_actioned_at
                FROM
                     (
                         -- opens
                         SELECT co.mailing_id, um.subject_id, 
                                CASE WHEN d.id   IS NULL THEN 0 ELSE d.id   END AS email_domain_id,
                                CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                                COUNT(1) AS opens,
                                COUNT(DISTINCT co.user_id) AS opens_unique,
                                0 AS clicks,
                                0 AS clicks_unique,
                                0 AS actions,
                                0 AS new_members,
                                0 AS unsubs,
                                0 AS unsub_complaints,
                                0 AS unsub_bounces,
                                0 AS donations,
                                0 AS revenue,
                                mme.max_opened_at,
                                mme.max_clicked_at,
                                mme.max_actioned_at
                           FROM 
                                mailing_max_events mme,
                                core_open co,
                                core_usermailing um,
                                domain_users du
                                LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                                source_users su
                                LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                          WHERE co.mailing_id = um.mailing_id
                            AND co.user_id = um.user_id
                            AND um.user_id = du.user_id
                            AND um.user_id = su.user_id
                            AND um.mailing_id = mme.mailing_id
                            AND co.mailing_id IN (#{mailing_ids})
                          GROUP BY co.mailing_id, um.subject_id, email_domain_id, tus.id,  mme. max_opened_at, mme.max_clicked_at, mme.max_actioned_at

                     UNION

                         -- clicks
                         SELECT cc.mailing_id, um.subject_id, 
                                CASE WHEN d.id   IS NULL THEN 0 ELSE d.id   END AS email_domain_id,
                                CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                                0 AS opens,
                                0 AS opens_unique,
                                COUNT(1) AS clicks,
                                COUNT(DISTINCT cc.user_id) AS clicks_unique,
                                0 AS actions,
                                0 AS new_members,
                                0 AS unsubs,
                                0 AS unsub_complaints,
                                0 AS unsub_bounces,
                                0 AS donations,
                                0 AS revenue,
                                mme.max_opened_at,
                                mme.max_clicked_at,
                                mme.max_actioned_at
                           FROM mailing_max_events mme,
                                core_click cc,
                                core_clickurl ccu, 
                                core_usermailing um,
                                domain_users du
                                LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                                source_users su
                                LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                          WHERE cc.mailing_id = um.mailing_id
                            AND cc.user_id = um.user_id
                            AND um.user_id = du.user_id
                            AND um.user_id = su.user_id
                            AND cc.clickurl_id = ccu.id
                            AND NOT ccu.url like '%/cms/unsubscribe/%'
                            AND NOT ccu.url like '%/act/unsubscribe/%'
                            AND um.mailing_id = mme.mailing_id
                            AND cc.mailing_id IN (#{mailing_ids})
                          GROUP BY cc.mailing_id, um.subject_id, email_domain_id, tus.id,  mme. max_opened_at, mme.max_clicked_at, mme.max_actioned_at

                     UNION

                         -- actions
                         SELECT ca.mailing_id, um.subject_id, 
                                CASE WHEN d.id   IS NULL THEN 0 ELSE d.id   END AS email_domain_id,
                                CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                                0 AS opens,
                                0 AS opens_unique,
                                0 AS clicks,
                                0 AS clicks_unique,
                                COUNT(1) AS actions,
                                SUM(CASE WHEN ca.created_user = TRUE THEN 1 ELSE 0 END) AS new_members,
                                0 AS unsubs,
                                0 AS unsub_complaints,
                                0 AS unsub_bounces,
                                0 AS donations,
                                0 AS revenue,
                                mme.max_opened_at,
                                mme.max_clicked_at,
                                mme.max_actioned_at
                           FROM mailing_max_events mme,
                                core_action ca
                                LEFT OUTER JOIN core_unsubscribeaction cua ON ca.id = cua.action_ptr_id,
                                core_usermailing um,
                                domain_users du
                                LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                                source_users su
                                LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                          WHERE ca.status = 'complete'
                            AND ca.mailing_id = um.mailing_id
                            AND ca.user_id = um.user_id
                            AND um.user_id = du.user_id
                            AND um.user_id = su.user_id
                            AND cua.action_ptr_id IS NULL
                            AND um.mailing_id = mme.mailing_id
                            AND ca.mailing_id IN (#{mailing_ids})
                          GROUP BY ca.mailing_id, um.subject_id, email_domain_id, tus.id,  mme. max_opened_at, mme.max_clicked_at, mme.max_actioned_at

                     UNION

                         -- unsubs
                         SELECT ca.mailing_id, um.subject_id, 
                                CASE WHEN d.id   IS NULL THEN 0 ELSE d.id   END AS email_domain_id,
                                CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                                0 AS opens,
                                0 AS opens_unique,
                                0 AS clicks,
                                0 AS clicks_unique,
                                0 AS actions,
                                0 AS new_members,
                                SUM(CASE WHEN sct.name = 'unsubscribe' THEN 1 ELSE 0 END) AS unsubs,
                                SUM(CASE WHEN sct.name = 'unsubscribe_email' THEN 1 ELSE 0 END) AS unsub_complaints,
                                SUM(CASE WHEN sct.name = 'unsubscribe_bounce' THEN 1 ELSE 0 END) AS unsub_bounces,
                                0 AS donations,
                                0 AS revenue,
                                mme.max_opened_at,
                                mme.max_clicked_at,
                                mme.max_actioned_at
                           FROM mailing_max_events mme,
                                core_action ca,
                                core_unsubscribeaction cua,
                                core_subscriptionhistory sh,
                                core_subscriptionchangetype sct,
                                core_usermailing um,
                                domain_users du
                                LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                                source_users su
                                LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                          WHERE ca.status = 'complete'
                            AND ca.mailing_id = um.mailing_id
                            AND cua.action_ptr_id = ca.id
                            AND ca.id = sh.action_id
                            AND um.user_id = ca.user_id
                            AND um.user_id = sh.user_id 
                            AND um.user_id = du.user_id
                            AND um.user_id = su.user_id
                            AND sh.change_id = sct.id
                            AND um.mailing_id = mme.mailing_id
                            AND ca.mailing_id IN (#{mailing_ids})
                          GROUP BY ca.mailing_id, um.subject_id, email_domain_id, tus.id,  mme. max_opened_at, mme.max_clicked_at, mme.max_actioned_at

                     UNION

                         -- donations
                         SELECT ca.mailing_id, um.subject_id, 
                                CASE WHEN d.id   IS NULL THEN 0 ELSE d.id   END AS email_domain_id,
                                CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                                0 AS opens,
                                0 AS opens_unique,
                                0 AS clicks,
                                0 AS clicks_unique,
                                0 AS actions,
                                0 AS new_members,
                                0 AS unsubs,
                                0 AS unsub_complaints,
                                0 AS unsub_bounces,
                                COUNT(1) AS donations,
                                SUM(co.total) AS revenue,
                                mme.max_opened_at,
                                mme.max_clicked_at,
                                mme.max_actioned_at
                           FROM mailing_max_events mme,
                                core_donationaction cda,
                                core_action ca,
                                core_order co,
                                core_transaction ct,
                                core_usermailing um,
                                domain_users du
                                LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                                source_users su
                                LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                          WHERE cda.action_ptr_id = ca.id
                            AND ca.id = co.action_id
                            AND co.id = ct.order_id
                            AND ca.status = 'complete'
                            AND co.status = 'completed'
                            AND ct.status = 'completed'
                            AND ct.success = true
                            AND ca.mailing_id = um.mailing_id
                            AND ca.user_id = um.user_id
                            AND um.user_id = du.user_id
                            AND um.user_id = su.user_id
                            AND um.mailing_id = mme.mailing_id
                            AND ca.mailing_id IN (#{mailing_ids})
                          GROUP BY ca.mailing_id, um.subject_id, email_domain_id, tus.id,  mme. max_opened_at, mme.max_clicked_at, mme.max_actioned_at

               ) AS agg
               GROUP BY mailing_id, subject_id, email_domain_id, source_id, max_opened_at, max_clicked_at, max_actioned_at
               ORDER BY mailing_id, subject_id, email_domain_id, source_id
    }

    Apartment::Database.process(client.cc_schema) do
      ActiveRecord::Base.transaction do
        Apartment.connection.execute(sql1)
        result = Apartment.connection.execute(sql2)
        # puts "mailing_ids= #{mailing_ids}  rows inserted= #{result.cmd_tuples}"
      end
    end
    puts "        Summary::Populate.email_action_summary(#{client.ak_schema}) update_email_action_summary(): #{Time.now - t} seconds"
    puts "            mailing_ids: #{mailing_ids}"
  end


  def clean_up(client)
    t = Time.now

    # sql1 = %Q{ TRUNCATE #{client.cc_schema}.mailing_max_events}
    sql2 = %Q{ VACUUM ANALYZE #{client.cc_schema}.email_action_summary}

    Apartment::Database.process(client.cc_schema) do
      # Apartment.connection.execute(sql1)
      Apartment.connection.execute(sql2)
    end
    puts "        Summary::Populate.email_action_summary(#{client.ak_schema}) clean_up(): #{Time.now - t} seconds"
  end


  public


  def email_action_summary(client)

    populate_mailing_max_events(client)
    mailing_ids = determine_mailings_to_refresh(client)

    # Limit how many mailings are processed at once. 
    # The big query in update_email_action_summary()
    # seems to exponentially slow down when the number
    # of mailings grows and core_[open|click|action]
    # is large. 
    while ! mailing_ids.empty?
      update_email_action_summary(client, mailing_ids.shift(10))
    end

    clean_up(client)

  end

    end
  end
end
