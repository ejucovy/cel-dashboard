module Summary
  module Populate
    class << self

  private

  def sent_mailings(client)
    sql = %Q{
              SELECT cm.id
                FROM #{client.ak_schema}.core_mailing cm
                     LEFT OUTER JOIN (SELECT DISTINCT mailing_id FROM #{client.cc_schema}.email_sent_summary) AS ess ON cm.id = ess.mailing_id
               WHERE ess.mailing_id IS NULL 
                 AND cm.status in ('completed', 'stopped', 'died')
                ORDER BY cm.id
    }

    rows = []
    Apartment::Database.process(client.cc_schema) do
      rows = Apartment.connection.execute(sql)
    end

    mailing_ids = rows.collect{|r| r['id'].to_i}
  end


  public


  def email_sent_summary(client)

    mailing_ids = sent_mailings(client)

    return if mailing_ids.empty?

    sql = %Q{ INSERT INTO #{client.cc_schema}.email_sent_summary (mailing_id, subject_id, email_domain_id, user_source_id, sent)
              SELECT um.mailing_id, subject_id, 
                     CASE WHEN td.id IS NULL THEN 0 ELSE td.id END AS email_domain_id,
                     CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS user_source_id,
                     COUNT(1) cnt
                FROM #{client.cc_schema}.domain_users du
                     LEFT OUTER JOIN #{client.cc_schema}.top_email_domains td ON du.domain_id = td.id,
                     #{client.cc_schema}.source_users su
                     LEFT OUTER JOIN #{client.cc_schema}.top_user_sources tus ON su.source_id = tus.id,
                     #{client.ak_schema}.core_usermailing um,
                     #{client.ak_schema}.core_mailing cm
               WHERE cm.id IN ( #{ mailing_ids.join(',') } )
                 AND cm.id = um.mailing_id
                 AND um.user_id = du.user_id
                 AND um.user_id = su.user_id
               GROUP BY um.mailing_id, um.subject_id, email_domain_id, tus.id
               ORDER BY um.mailing_id, um.subject_id, email_domain_id, tus.id
    }
    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end

    end
  end
end
