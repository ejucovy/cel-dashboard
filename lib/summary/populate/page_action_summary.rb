module Summary
  module Populate
    class << self

  private

  def determine_pages_to_refresh(client)
    sql = %Q{  SELECT DISTINCT ca1.page_id
                 FROM core_action ca1
                      LEFT OUTER JOIN page_action_summary pas ON ca1.page_id = pas.page_id
                WHERE pas.page_id IS NULL

               UNION

               SELECT DISTINCT ca2.page_id
                 FROM 
                      (
                       SELECT MAX(id) AS max_action_id, page_id 
                         FROM core_action
                        GROUP BY page_id
                      ) AS ca2,
                      page_action_summary pas
                WHERE ca2.page_id = pas.page_id
                  AND ca2.max_action_id > pas.max_action_id
    }

    page_ids = []
    Apartment::Database.process(client.cc_schema) do
      page_ids = Apartment.connection.execute(sql).collect{|r| r['page_id'].to_i}
    end
    page_ids.sort
  end


  def update_page_action_summary(client, page_ids)

    page_ids = page_ids.join(', ')

    sql1 = %Q{ DELETE FROM page_action_summary WHERE page_id IN (#{page_ids}) }

    sql2 = %Q{ INSERT INTO page_action_summary
                            (
                              page_id,
                              mailing_id, subject_id,
                              user_source_id, action_source_id,
                              actions,
                              new_members, new_subscribers,
                              revenue,
                              max_action_id
                            )
                 SELECT pas.page_id,
                        pas.mailing_id, pas.subject_id,
                        pas.user_source_id, pas.action_source_id,
                        pas.actions,
                        pas.new_members, pas.new_subscribers,
                        pas.revenue,
                        ma.max_action_id
                   FROM 
                        (
                          SELECT ca.page_id, 
                                 ca.mailing_id, um.subject_id,
                                 CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS user_source_id,
                                 CASE WHEN tas.id IS NULL THEN 0 ELSE tas.id END AS action_source_id,
                                 COUNT(1) AS actions,
                                 SUM(CASE WHEN ca.created_user = TRUE THEN 1 ELSE 0 END) AS new_members,
                                 SUM(CASE WHEN ca.subscribed_user = TRUE THEN 1 ELSE 0 END) AS new_subscribers,
                                 SUM(CASE WHEN co.id IS NULL THEN 0 ELSE co.total END) AS revenue
                            FROM core_action ca
                                 LEFT OUTER JOIN core_order co ON ca.id = co.action_id AND co.status = 'completed'
                                 LEFT OUTER JOIN core_usermailing um ON ca.mailing_id = um.mailing_id AND ca.user_id = um.user_id
                                 LEFT OUTER JOIN top_action_sources tas ON tas.source = ca.source,
                                 source_users su
                                 LEFT OUTER JOIN top_user_sources tus ON tus.id = su.source_id
                           WHERE ca.user_id = su.user_id
                             AND ca.page_id IN (#{page_ids})
                           GROUP BY ca.page_id, ca.mailing_id, um.subject_id, tus.id, tas.id
                           ORDER BY ca.page_id, ca.mailing_id, um.subject_id
                        ) AS pas,
                        (
                          SELECT MAX(id) AS max_action_id, page_id 
                            FROM core_action
                           GROUP BY page_id
                        ) AS ma
                  WHERE pas.page_id = ma.page_id
    }

    Apartment::Database.process(client.cc_schema) do
      ActiveRecord::Base.transaction do
        Apartment.connection.execute(sql1)
        result = Apartment.connection.execute(sql2)
        puts "page_ids= #{page_ids}  rows inserted= #{result.cmd_tuples}" if Rails.env == 'development'
      end
    end
  end


  def clean_up(client)
    sql = %Q{ VACUUM ANALYZE #{client.cc_schema}.page_action_summary}

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end


  public


  def page_action_summary(client)

    page_ids = determine_pages_to_refresh(client)

    # Limit how many pages are processed at once
    # to keep the query from exponentlially
    # blowing up.
    while ! page_ids.empty?
      update_page_action_summary(client, page_ids.shift(10))
    end

    clean_up(client)

  end

    end
  end
end
