module Summary
  module Populate
    class << self

  private

  def email_action_summary_by_period_chunk(client, type, data)
    opens_where = clicks_where = actions_where = ''

    if type == 'mailing'
      opens_where   = "AND co.mailing_id = #{data}"
      clicks_where  = "AND cc.mailing_id = #{data}"
      actions_where = "AND ca.mailing_id = #{data}"
    else
      opens_where   = "AND co.created_at >= '#{data}'"
      clicks_where  = "AND cc.created_at >= '#{data}'"
      actions_where = "AND ca.created_at >= '#{data}'"
    end


    sql = %Q{ INSERT INTO email_action_summary_by_period (period, 
                                                          mailing_id, subject_id, email_domain_id, user_source_id, 
                                                          opens, clicks, actions, new_members, 
                                                          unsubs, unsub_complaints, unsub_bounces, 
                                                          donations, revenue)
              SELECT period, 
                     mailing_id, subject_id, email_domain_id, source_id,
                     SUM(opens)            AS opens,
                     SUM(clicks)           AS clicks,
                     SUM(actions)          AS actions,
                     SUM(new_members)      AS new_members,
                     SUM(unsubs)           AS unsubs,
                     SUM(unsub_complaints) AS unsub_complaints,
                     SUM(unsub_bounces)    AS unsub_bounces,
                     SUM(donations)        AS donations,
                     SUM(revenue)          AS revenue
                FROM
                     (
                        -- opens
                        SELECT tstrunc_5m(co.created_at) AS period,
                               co.mailing_id, um.subject_id, 
                               CASE WHEN d.id IS NULL THEN 0 ELSE d.id END AS email_domain_id,
                               CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                               COUNT(1) AS opens,
                               0 AS clicks,
                               0 AS actions,
                               0 AS new_members,
                               0 AS unsubs,
                               0 AS unsub_complaints,
                               0 AS unsub_bounces,
                               0 AS donations,
                               0 AS revenue
                          FROM core_open co,
                               core_usermailing um,
                               domain_users du
                               LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                               source_users su
                               LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                         WHERE co.mailing_id = um.mailing_id
                           AND co.user_id = um.user_id
                           AND um.user_id = du.user_id
                           AND um.user_id = su.user_id
                           #{opens_where}
                         GROUP BY period, co.mailing_id, um.subject_id, email_domain_id, tus.id

                      UNION

                        -- clicks
                        SELECT tstrunc_5m(cc.created_at) AS period,
                               cc.mailing_id, um.subject_id, 
                               CASE WHEN d.id IS NULL THEN 0 ELSE d.id END AS email_domain_id,
                               CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                               0 AS opens,
                               COUNT(1) AS clicks,
                               0 AS actions,
                               0 AS new_members,
                               0 AS unsubs,
                               0 AS unsub_complaints,
                               0 AS unsub_bounces,
                               0 AS donations,
                               0 AS revenue
                         FROM  core_click cc,
                               core_clickurl ccu, 
                               core_usermailing um,
                               domain_users du
                               LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                               source_users su
                               LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                         WHERE cc.mailing_id = um.mailing_id
                           AND cc.user_id = um.user_id
                           AND um.user_id = du.user_id
                           AND um.user_id = su.user_id
                           AND cc.clickurl_id = ccu.id
                           AND NOT ccu.url like '%/cms/unsubscribe/%'
                           AND NOT ccu.url like '%/act/unsubscribe/%'
                           #{clicks_where}
                         GROUP BY period, cc.mailing_id, um.subject_id, email_domain_id, tus.id
                      UNION

                        -- actions
                        SELECT tstrunc_5m(CASE WHEN ca.created_at < um.created_at THEN ca.updated_at ELSE ca.created_at END) AS period,
                               ca.mailing_id, um.subject_id, 
                               CASE WHEN d.id IS NULL THEN 0 ELSE d.id END AS email_domain_id,
                               CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                               0 AS opens,
                               0 AS clicks,
                               COUNT(1) AS actions,
                               SUM(CASE WHEN ca.created_user = TRUE THEN 1 ELSE 0 END) AS new_members,
                               0 AS unsubs,
                               0 AS unsub_complaints,
                               0 AS unsub_bounces,
                               0 AS donations,
                               0 AS revenue
                          FROM core_action ca
                               LEFT OUTER JOIN core_unsubscribeaction cua ON ca.id = cua.action_ptr_id,
                               core_usermailing um,
                               domain_users du
                               LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                               source_users su
                               LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                         WHERE ca.status = 'complete'
                           AND ca.mailing_id = um.mailing_id
                           AND ca.user_id = um.user_id
                           AND um.user_id = du.user_id
                           AND um.user_id = su.user_id
                           AND cua.action_ptr_id IS NULL
                           #{actions_where}
                         GROUP BY period, ca.mailing_id, um.subject_id, email_domain_id, tus.id

                      UNION

                        -- unsubs
                        SELECT tstrunc_5m(CASE WHEN ca.created_at < um.created_at THEN ca.updated_at ELSE ca.created_at END) AS period,
                               ca.mailing_id, um.subject_id, 
                               CASE WHEN d.id IS NULL THEN 0 ELSE d.id END AS email_domain_id,
                               CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                               0 AS opens,
                               0 AS clicks,
                               0 AS actions,
                               0 AS new_members,
                               SUM(CASE WHEN sct.name = 'unsubscribe' THEN 1 ELSE 0 END)        AS unsubs,
                               SUM(CASE WHEN sct.name = 'unsubscribe_email' THEN 1 ELSE 0 END)  AS unsub_complaints,
                               SUM(CASE WHEN sct.name = 'unsubscribe_bounce' THEN 1 ELSE 0 END) AS unsub_bounces,
                               0 AS donations,
                               0 AS revenue
                          FROM core_action ca,
                               core_unsubscribeaction cua,
                               core_subscriptionhistory sh,
                               core_subscriptionchangetype sct,
                               core_usermailing um,
                               domain_users du
                               LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                               source_users su
                               LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                         WHERE ca.status = 'complete'
                           AND ca.mailing_id = um.mailing_id
                           AND cua.action_ptr_id = ca.id
                           AND ca.id = sh.action_id
                           AND um.user_id = ca.user_id
                           AND um.user_id = sh.user_id 
                           AND um.user_id = du.user_id
                           AND um.user_id = su.user_id
                           AND sh.change_id = sct.id
                           #{actions_where}
                        GROUP BY period, ca.mailing_id, um.subject_id, email_domain_id, tus.id

                      UNION

                        -- donations
                        SELECT tstrunc_5m(CASE WHEN ca.created_at < um.created_at THEN ca.updated_at ELSE ca.created_at END) AS period,
                               ca.mailing_id, um.subject_id, 
                               CASE WHEN d.id IS NULL THEN 0 ELSE d.id END AS email_domain_id,
                               CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS source_id,
                               0 AS opens,
                               0 AS clicks,
                               0 AS actions,
                               0 AS new_members,
                               0 AS unsubs,
                               0 AS unsub_complaints,
                               0 AS unsub_bounces,
                               COUNT(1) AS donations,
                               SUM(co.total) AS revenue
                          FROM core_donationaction cda,
                               core_action ca,
                               core_order co,
                               core_transaction ct,
                               core_usermailing um,
                               domain_users du
                               LEFT OUTER JOIN top_email_domains d on d.id = du.domain_id,
                               source_users su
                               LEFT OUTER JOIN top_user_sources tus on tus.id = su.source_id
                         WHERE cda.action_ptr_id = ca.id
                           AND ca.id = co.action_id
                           AND co.id = ct.order_id
                           AND ca.status = 'complete'
                           AND co.status = 'completed'
                           AND ct.status = 'completed'
                           AND ct.success = true
                           AND ca.mailing_id = um.mailing_id
                           AND ca.user_id = um.user_id
                           AND um.user_id = du.user_id
                           AND um.user_id = su.user_id
                           #{actions_where}
                         GROUP BY period, ca.mailing_id, um.subject_id, email_domain_id, tus.id
              ) AS agg
               GROUP BY period, mailing_id, subject_id, email_domain_id, source_id
               ORDER BY period, mailing_id, subject_id, email_domain_id, source_id
    }
    Apartment::Database.process(client.cc_schema) do
      result = Apartment.connection.execute(sql)
      puts "type= #{type}  data= #{data}  rows inserted= #{result.cmd_tuples}" if Rails.env == 'development'
    end
  end


  public


  #
  # The latest AK sync will almost certainly have additional data
  # for the last period in the summary table. The easist way to 
  # ensure that the last period in summary table is complete is
  # to delete it and re-populate.
  #
  def email_action_summary_by_period(client)
    sql1 = %Q{ SELECT MAX(period) max_period FROM #{client.cc_schema}.email_action_summary_by_period }

    sql2 = %Q{ DELETE FROM #{client.cc_schema}.email_action_summary_by_period WHERE period = ? }

    max_period = nil
    Apartment::Database.process(client.cc_schema) do
      max_period = Apartment.connection.execute(sql1).first['max_period']

      if max_period

        ActiveRecord::Base.transaction do
          sql2 = ActiveRecord::Base.escape_sql([sql2, max_period])
          result = Apartment.connection.execute(sql2)
          puts "rows deleted= #{result.cmd_tuples}" if Rails.env == 'development'
          email_action_summary_by_period_chunk(client, 'timestamp', max_period)
        end

      else

        sent_mailings(client).each do |id|
          email_action_summary_by_period_chunk(client, 'mailing', id)
        end
      end
    end
  end

    end
  end
end
