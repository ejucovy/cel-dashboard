module Summary
  module Populate
    class << self

  def list_size_by_period(client)

    sql1 = %Q{ TRUNCATE #{client.cc_schema}.list_size_by_period RESTART IDENTITY }

    sql2 = %Q{
              INSERT INTO list_size_by_period (period, list_id,
                                               subs, unsubs,
                                               total_subs,
                                               total_unsubs,
                                               mailable)
              SELECT period, 
                     list_id,
                     subs,
                     unsubs,
                     SUM(subs)   OVER (PARTITION BY list_id ORDER BY period) AS total_subs,
                     SUM(unsubs) OVER (PARTITION BY list_id ORDER BY period) AS tota_unsubs,
                     SUM(subs)   OVER (PARTITION BY list_id ORDER BY period) - SUM(unsubs) OVER (PARTITION BY list_id ORDER BY period) AS mailable
                FROM 
                     (
                      SELECT csh.list_id,
                             tstrunc_5m(csh.created_at) AS period,
                             SUM(CASE WHEN POSITION('subscribe'   in csct.name) = 1 THEN 1 ELSE 0 END) AS subs,
                             SUM(CASE WHEN POSITION('unsubscribe' in csct.name) = 1 THEN 1 ELSE 0 END) AS unsubs
                        FROM core_subscriptionhistory csh,
                             core_subscriptionchangetype csct
                       WHERE csh.change_id = csct.id
                       GROUP BY csh.list_id, period
                     ) AS data
               ORDER BY period, list_id
    }

    Apartment::Database.process(client.cc_schema) do
      ActiveRecord::Base.transaction do
        Apartment.connection.execute(sql1)
        Apartment.connection.execute(sql2)
      end
    end
  end

    end
  end
end
