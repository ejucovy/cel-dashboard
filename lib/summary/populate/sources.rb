module Summary
  module Populate
    class << self

  # 
  # User sources
  #
  def sources_from_users(client)
    sql = %Q{ INSERT INTO #{client.cc_schema}.sources (source, created_at)
              SELECT cus.source, LOCALTIMESTAMP
                FROM 
                    (
                     SELECT distinct source
                       FROM #{client.ak_schema}.core_user
                    ) AS cus
                    LEFT OUTER JOIN #{client.cc_schema}.sources s ON cus.source = s.source
              WHERE s.source IS NULL
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.sources")
    end
  end


  def source_users(client)
    sql = %Q{ INSERT INTO #{client.cc_schema}.source_users (source_id, user_id)
              SELECT s.id, cu.id 
                FROM #{client.cc_schema}.sources s,
                     #{client.ak_schema}.core_user cu
                     LEFT OUTER JOIN #{client.cc_schema}.source_users su ON cu.id = su.user_id
               WHERE s.source = cu.source
                 AND su.user_id IS NULL
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.source_users")
    end
  end


  def source_user_counts(client)
    sql = %Q{ UPDATE #{client.cc_schema}.sources AS s
                 SET user_count = users.cnt FROM (SELECT source_id, count(1) AS cnt
                                                    FROM #{client.cc_schema}.source_users 
                                                   GROUP BY source_id) AS users
               WHERE s.id = users.source_id;
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end

  # 
  # Action sources
  #
  def sources_from_actions(client)
    sql = %Q{ INSERT INTO #{client.cc_schema}.sources (source, created_at)
              SELECT cas.source, LOCALTIMESTAMP
                FROM 
                    (
                     SELECT distinct source
                       FROM #{client.ak_schema}.core_action
                    ) AS cas
                    LEFT OUTER JOIN #{client.cc_schema}.sources s ON cas.source = s.source
              WHERE s.source IS NULL
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.sources")
    end
  end


  def source_actions(client)
    sql = %Q{ INSERT INTO #{client.cc_schema}.source_actions (source_id, action_id)
              SELECT s.id, ca.id 
                FROM #{client.cc_schema}.sources s,
                     #{client.ak_schema}.core_action ca
                     LEFT OUTER JOIN #{client.cc_schema}.source_actions sa ON ca.id = sa.action_id
               WHERE s.source = ca.source
                 AND sa.action_id IS NULL
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
      Apartment.connection.execute("ANALYZE #{client.cc_schema}.source_actions")
    end
  end


  def source_action_counts(client)
    sql = %Q{ UPDATE #{client.cc_schema}.sources AS s
                 SET action_count = actions.cnt FROM (SELECT source_id, count(1) AS cnt
                                                       FROM #{client.cc_schema}.source_actions 
                                                      GROUP BY source_id) AS actions
               WHERE s.id = actions.source_id;
    }

    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end

    end
  end
end