module Summary
  module Populate
    class << self

  #
  # Due to a historic scripting error by WAWD, some AK instances
  # have duplicate rows in core_subscriptionhistory for
  # the same user_id and action_id (which should never happen).
  # This method deletes the duplicate rows.
  #
  def cleanup_subscriptionhistory(client)
    sql = %q{ DELETE FROM core_subscriptionhistory
               WHERE core_subscriptionhistory.id 
                  IN (
                      SELECT csh.id
                        FROM core_subscriptionhistory csh,
                             (
                              SELECT MIN(id) AS id, csh_b.action_id, csh_b.user_id
                                FROM core_subscriptionhistory csh_b,
                                     (       
                                      SELECT csh_a.action_id, csh_a.user_id
                                        FROM core_subscriptionhistory csh_a
                                       GROUP BY csh_a.user_id, csh_a.action_id
                                      HAVING COUNT(1) > 1
                                     ) AS dupes
                               WHERE csh_b.user_id = dupes.user_id
                                 AND csh_b.action_id = dupes.action_id
                               GROUP BY csh_b.action_id, csh_b.user_id
                             ) AS min_csh
                      WHERE csh.user_id = min_csh.user_id
                        AND csh.action_id = min_csh.action_id
                        AND csh.id > min_csh.id
                     )
    }
    Apartment::Database.process(client.cc_schema) do
      Apartment.connection.execute(sql)
    end
  end

    end
  end
end
