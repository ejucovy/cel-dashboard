module Summary
  module Populate
    class << self

  def record_page_view(client, page_id, referrer_id, browser_spec, akid)
    t = Time.now.utc
    period = Time.at((t.to_f / (5*60)).floor * (5*60)) # truncate time to 5 min interval

    mid = 0
    if akid.present?
      a = akid.split('.')
      mid = (a.size == 3 ? a[0].to_i : 0)
    end

    Apartment::Database.switch(client.cc_schema_name)
    ActiveRecord::Base.transaction do

      update_sql = %Q{
        UPDATE page_view_summary_by_period
           SET views = views + 1
         WHERE period = ?
           AND page_id = ?
           AND mailing_id = ?
           AND referrer_url_id = ?
           AND browser_spec_id = ?
      }

      row_cnt = ActiveRecord::Base.connection.update(ActiveRecord::Base.escape_sql([update_sql, period.to_s(:db), page_id, mid, referrer_id, browser_spec.id]))

      if row_cnt == 0
        insert_sql = %Q{
          INSERT INTO page_view_summary_by_period
            (period, page_id, mailing_id, referrer_url_id, browser_spec_id, views)
          VALUES (?, ?, ?, ?, ?, 1)
        }
        ActiveRecord::Base.connection.execute(ActiveRecord::Base.escape_sql([insert_sql, period.to_s(:db), page_id, mid, referrer_id, browser_spec.id]))
      end
    end
    Apartment::Database.switch(nil)
  end


    end
  end
end
