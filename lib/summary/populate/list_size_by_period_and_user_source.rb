module Summary
  module Populate
    class << self

  def list_size_by_period_and_user_source(client)

    #
    # Saving code in case it is useful in the future
    #

    # sql1 = %Q{ TRUNCATE #{client.cc_schema}.list_size_by_period RESTART IDENTITY }

    # sql2 = %Q{
    #           INSERT INTO list_size_by_period_and_user_source (period, list_id, user_source_id,
    #                                            subs, unsubs,
    #                                            user_source_subs,
    #                                            total_user_source_subs,
    #                                            user_source_unsubs,
    #                                            total_user_source_unsubs,
    #                                            user_source_mailable,
    #                                            total_mailable)
    #           SELECT period, 
    #                  list_id, user_source_id,
    #                  subs,
    #                  unsubs,
    #                  SUM(subs)   OVER (PARTITION BY list_id, user_source_id ORDER BY period) AS user_source_subs,
    #                  SUM(subs)   OVER (PARTITION BY list_id ORDER BY period) AS total_user_source_subs,
    #                  SUM(unsubs) OVER (PARTITION BY list_id, user_source_id ORDER BY period) AS user_source_unsubs,
    #                  SUM(unsubs) OVER (PARTITION BY list_id ORDER BY period) AS total_user_source_unsubs,
    #                  SUM(subs)   OVER (PARTITION BY list_id, user_source_id ORDER BY period) - SUM(unsubs) OVER (PARTITION BY list_id, user_source_id ORDER BY period) AS user_source_mailable,
    #                  SUM(subs)   OVER (PARTITION BY list_id ORDER BY period) - SUM(unsubs) OVER (PARTITION BY list_id ORDER BY period) AS total_mailable
    #             FROM 
    #                  (
    #                   SELECT csh.list_id,
    #                          CASE WHEN tus.id IS NULL THEN 0 ELSE tus.id END AS user_source_id,
    #                          tstrunc_5m(csh.created_at) AS period,
    #                          SUM(CASE WHEN POSITION('subscribe'   in csct.name) = 1 THEN 1 ELSE 0 END) AS subs,
    #                          SUM(CASE WHEN POSITION('unsubscribe' in csct.name) = 1 THEN 1 ELSE 0 END) AS unsubs
    #                     FROM user_source_users usu
    #                          LEFT OUTER JOIN top_user_sources tus ON usu.user_source_id = tus.id,
    #                          core_subscriptionhistory csh,
    #                          core_subscriptionchangetype csct
    #                    WHERE csh.user_id = usu.user_id
    #                      AND csh.change_id = csct.id
    #                    GROUP BY csh.list_id, tus.id, period
    #                  ) AS data
    #            ORDER BY period, list_id, user_source_id
    # }

    # Apartment::Database.process(client.cc_schema) do
    #   ActiveRecord::Base.transaction do
    #     Apartment.connection.execute(sql1)
    #     Apartment.connection.execute(sql2)
    #   end
    # end

  end

    end
  end
end
