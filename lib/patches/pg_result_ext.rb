module PG
  class Result

    def rows
      self.cmd_tuples
    end
    alias_method :size, :rows

  end
end
