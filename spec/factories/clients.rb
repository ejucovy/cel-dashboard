# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client do
    active false
    admin false
    name "MyString"
    schema_name "MyString"
    user_id 1
  end
end
